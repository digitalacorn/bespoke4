<?php
	if (!defined('ABSPATH')) exit;

class BespokeCarousel {
	private static $enqueued_js = false;
	private static $js_init_content = '';
	public static function activate($element, $options=array()) {
		global $bespoke;
		if (!self::$enqueued_js) {
			wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array(), $bespoke->version('parent'), true );		
			self::$enqueued_js = true;
			add_action( 'bespoke_do_js_docready', array('BespokeCarousel', 'do_js_docready')); 
		}
		if (!empty($element)) {
			self::$js_init_content .= ';jQuery('.json_encode($element).').slick('.json_encode($options).')';
		}
	}
	public static function fullscreengalleries() {
		add_action('bespoke_do_after_page', array('BespokeCarousel', 'do_after_page'));
		self::activate('');
		self::$js_init_content .= ';var da_fs_gal = da_bespoke.fsGallery(); da_fs_gal.init({container: "#fullscreengallery"});';
	}

	public static function do_after_page() {
		?>
		<div id="fullscreengallery">
			<div class="container">
				<div class="close"><a href="#"><i class="fa fa-times-circle"></i></a></div>
				<div class="slickwrapper"></div>
			</div>
		</div>
		<?php
	}
	public static function do_js_docready() {
		echo self::$js_init_content;
	}
}



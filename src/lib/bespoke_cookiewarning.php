<?php

	if (!defined('ABSPATH')) exit;

	if (is_admin())
	{
		
		add_action('admin_menu', 'da_add_cookie_policy_menu', 11);
		function da_add_cookie_policy_menu(){
			add_submenu_page('bespoke', 'Cookie Policy Settings', 'Cookie Policy', 'manage_options', 'bespoke_cookiepolicy', 'da_cookie_settings_content'); 
		}

		function da_cookie_settings_content(){
			if (!current_user_can('manage_options'))
				wp_die( __('You do not have sufficient permissions to access this page.') );
		
			$page_list = get_pages();
			$page_options = array();
			foreach ($page_list as $pages)
				$page_options[get_page_uri($pages->ID)] = $pages->post_title;

			$fields = array(
				'show_warning' => array(
						'name'=>"Display a cookie warning",
						'description'=>'Your site may use cookies. If you have google analytics installed - it definitely does.'.
										'<br/>Legislation exists which requires that you inform your visitors of this.',
						'input_type'=>'checkbox'
						),
				'initial_text' => array(
						'name'=>"The text to display",
						'description'=>'Check with a lawyer for a suitable phrase to use in this area.',
						'input_type'=>'textarea',
						),
				'read_more' => array(
						'name'=>'Use a "read more" link',
						'description'=>'Check this box to enable a \'read more\' link',
						'input_type'=>'checkbox'
						),
				'read_more_url' => array(
						'name'=>"Read More Link",
						'description'=>'Select your T&amp;C\'s or cookie-policy page below',
						'input_type'=>'select',
						'options'=>$page_options
						)
			);
			echo '<div class="bespokesettings"><h1>Cookie Policy Settings</h1>';
				da_render_global_option_inputs($fields);
			echo '</div>';	
		}
	
		// save links
		function da_save_cookie_options($field, $newvalue){
			update_option(da_meta_field($field), $newvalue);
			return $newvalue;
		}	

	}

	$show_warning = da_get_option('show_warning');

	if (in_array($show_warning, array("1", "show")))
	{
		add_action('wp_enqueue_scripts', 'bespoke_cookie_warning_scripts');
		if (!isset($_COOKIE['bespoke_accpeted_policy']))
			add_action( 'bespoke_do_js_docready', 'bespoke_do_cookie_js'); 
	}
	
	function bespoke_cookie_warning_scripts(){
		global $bespoke;
		wp_register_script("jquery-cookie", get_template_directory_uri().'/js/jquery.cookie.min.js', array('jquery'), $bespoke->version('parent'), true);
		wp_enqueue_script("jquery-cookie");
	}
	
	function bespoke_do_cookie_js(){
		$form = '';
		$output = '';
		$form .= '<form name="cookie_accept" id="cookie_accept" action="#">';
		$form .= '<input type="submit" name="accept" id="accept" value="' . apply_filters('bespoke_f_cookie_policy_input_value', 'ACCEPT') . '" />';
		$form .= '</form>';
		$initial_text = da_get_option('initial_text');
		$read_more = da_get_option('read_more');
		$read_more_url = get_site_url(null, da_get_option('read_more_url'));
		
		$output .= '<div class="initial_text">' . $initial_text . '</div>';
		
		if (in_array($read_more, array("1", "show"))) 
			$output .= '<a class="read_more" href="' . $read_more_url . '">read more</a>';
			
		$output .= $form;
		$opts = array(
			'content' => $output,
			'cookie' => 'bespoke_accpeted_policy'
		);

		echo 'da_bespoke.cookie_warning($, '.json_encode($opts).');';
	}		

	add_action('bespoke_do_dynamic_css', 'cookie_warning_css');	
	if (!function_exists('cookie_warning_css')) {
		function cookie_warning_css(){
		?>
#da_cookie_policy {background: #F1F1F1; border-bottom: 1px solid #E8E8E8; margin: 0; height: auto; display: block; top: auto; bottom: 0; width: 100%; left: 0; text-align: center; z-index: 99999 !important; position: fixed;}
#da_cookie_policy #cookie_policy_inner {padding: 10px; line-height: 30px;}
#da_cookie_policy div.initial_text {display: inline; padding: 0 0 0 20px;}
#da_cookie_policy form#cookie_accept {display: inline;}
#da_cookie_policy form#cookie_accept input{margin: 0 0 0 20px;}
#da_cookie_policy form#cookie_accept input:hover {cursor: pointer;}
#da_cookie_policy a.read_more {padding: 0 20px;}
#da_cookie_policy a.read_more:hover {}
body.has_cookie_bar { padding-bottom: 50px; }
		<?php		
		}
	}

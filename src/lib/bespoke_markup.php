<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package bespoke
 */

	function bespoke_content_template($name=null, $pt='') {
		$slug = 'template-parts/content';
		
		//return get_template_part($slug, $name);

		$ispformat = !$name && get_post_format();
		$name = $name ? (string) $name : get_post_format();
		$pt = $pt ? $pt : get_post_type();

		/* taken from get_template_part, but includes a post-type lookup, and 'loop' prefix */

		do_action( "get_template_part_{$slug}", $slug, ($name ? "{$name}-{$pt}" : $pt) );	 

		
		$templates = array();
		if ($name) {
			if (!is_single()) {
				$templates[] = "{$slug}-{$name}-loop-{$pt}.php";    	
			}
			$templates[] = "{$slug}-{$name}-{$pt}.php";
			if (!is_single() && $ispformat) {
				$templates[] = "{$slug}-{$name}-loop.php";	    	
			}
		}
		if (!is_single()) {
			$templates[] = "{$slug}-loop-{$pt}.php";	    	
		}
		if ($name) {
			$templates[] = "{$slug}-{$name}.php";
		}
		$templates[] = "{$slug}-{$pt}.php";
		$templates[] = "{$slug}.php";



		$found = locate_template($templates, true, false);

		if (WP_DEBUG) {
			$found = str_replace(get_theme_root(), 'themes', $found);
			echo '<!-- bespoke_content_template requested: '.esc_html($name).
					"\nSearching:\n".join("\n", $templates).
					"\nFound: ".$found.' -->';
		}
	}

	function bespoke_comments_template() {
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;		
	}

	function bespoke_classes($zone, $default=null) {
		$default = $default ? : array();
		$classes = apply_filters('bespoke_f_classes_'.$zone, $default);
		$classes = is_array($classes) ? $classes : array((string) $classes);
		echo esc_attr(join(' ', $classes));
	}

	function bespoke_render() {
		require_once (get_template_directory() . '/index.php');
	}

	function da_markup_responder_image($w, $h, $attr='') {
		$v = DAhash($w.'-'.$h, 'responder_image');
		return sprintf('<img %s src="%s/bespoke/responder.png?width=%d&amp;height=%d&amp;verify=%s" alt="" />', $attr, get_site_url(), $w, $h, $v);
	}

if ( ! function_exists( 'bespoke_markup_blog_page_header' ) ) :
	/* pluggable header appearing on the blog page - this is only if there is a page set for posts */
	function bespoke_markup_blog_page_header() {
		?>
		<header>
			<h1 class="page-title"><?php single_post_title(); ?></h1>
		</header>		
		<?php
	}	
endif;


if ( ! function_exists( 'bespoke_markup_search_page_header' ) ) :
	/* pluggable header appearing on the search-results page */

	function bespoke_markup_search_page_header() {
		?>
		<header class="page-header">
			<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'bespoke' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		</header><!-- .page-header -->
		<?php
	}	
endif;

if ( ! function_exists( 'bespoke_markup_archive_page_header' ) ) :
	/* pluggable header appearing on a generic archive page */

	function bespoke_markup_archive_page_header() {
		?>
		<header class="page-header">
			<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>
		</header><!-- .page-header -->
		<?php
	}	
endif;

if ( ! function_exists( 'bespoke_markup_post_date' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function bespoke_markup_post_date($linkpost=false) {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		if ($linkpost) {
			$txt = '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>';
		}
		else {
			$txt =  $time_string;		
		}

		$posted_on = sprintf( esc_html_x( 'Posted on %s', 'post date', 'bespoke' ),$txt );

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'bespoke_markup_author' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function bespoke_markup_author($linkauthor=false) {
		if ($linkauthor) {
			$txt = '<a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>';
		}
		else {
			$txt =  '<span class="fn n">'.esc_html( get_the_author() ).'</span>';		
		}
		$byline = sprintf(
			esc_html_x( 'by %s', 'post author', 'bespoke' ),'<span class="author vcard">'.$txt.'</span>'
		);
		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.
	}
endif;

if ( ! function_exists( 'bespoke_markup_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function bespoke_markup_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'bespoke' ) );
			if ( $categories_list && bespoke_categorized_blog() ) {
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'bespoke' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html__( ', ', 'bespoke' ) );
			if ( $tags_list ) {
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'bespoke' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( apply_filters('bespoke_f_comment_links_in_loop', false) && ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			/* translators: %s: post title */
			comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'bespoke' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
			echo '</span>';
		}

	}
endif;

if ( ! function_exists( 'bespoke_markup_edit_post' ) ) :
	function bespoke_markup_edit_post() {
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'bespoke' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'bespoke_markup_featured_image' ) ) :
	function bespoke_markup_featured_image( $size = 'thumbnail', $attr = array() ) {
		$thumbId = get_post_thumbnail_id( );
		$image = wp_get_attachment_image_src( $thumbId, $size );
		if ( $image ) {
			list($src, $width, $height, $is_intermediate) = $image;
			$orientation = ($width>=$height) ? 'landscape' : 'portrait';
			$default_attr = array('class'=>$orientation);
			$attr = wp_parse_args( $attr, $default_attr );
			the_post_thumbnail($size, $attr);
		} else {
			echo '<div class="missing_featured_image '.esc_attr($size).'"><span class="text">No Image</span></div>';
		}
	}
endif;

if ( ! function_exists( 'bespoke_markup_responded_image' ) ) :
	function bespoke_markup_responded_image( $width, $height, $size='medium_large', $thumbId=null ) {

		$thumbId = $thumbId ? $thumbId : get_post_thumbnail_id( );

		if ($thumbId) {
			list($url, $w, $h) = wp_get_attachment_image_src( $thumbId, $size );

			$alt = get_post_meta($thumbId, '_wp_attachment_image_alt', true);

			$style = isset($url) ? ' style="background: url(' . $url . ') 50% 50% no-repeat; background-size: cover;"' : '';
			echo '<div class="responded_image-wrap">';
				echo '<div class="responded_image" role="img" aria-label="'.esc_attr($alt).'" ' . $style . '>';
					echo da_markup_responder_image($width,$height);
				echo '</div>';
			echo '</div>';	
		}
	}
endif;	

function bespoke_markup_taxonomy_terms_links($post_id=null, $taxonomy_slug, $label) {
	if (is_null($post_id)){
		global $post;
		if (isset($post))
			$post_id = $post->ID;
		else
			return;
	}
	$out = array();	
	$terms = get_the_terms( $post_id, $taxonomy_slug );
	if ( ! empty( $terms ) ) {
		$out[] = '<div class="term_links '.$taxonomy_slug.'">'."\n";
		$out[] = sprintf( '<h2>%1$s</h2>', $label);
		$out[] = "\n".'<ul>'."\n";
		foreach ( $terms as $term ) {
			$out[] = sprintf( '<li><a href="%1$s">%2$s</a></li>',
				esc_url( get_term_link( $term->slug, $taxonomy_slug ) ),
				esc_html( $term->name )
			);
		}
		$out[] = "\n</ul></div>\n";
	}
	return implode( '', $out );
}

function bespoke_do_the_post_navigation() {
	$args = apply_filters('bespoke_f_the_post_navigation_args', array(
	            'prev_text'          => __( 'Older posts' ),
	            'next_text'          => __( 'Newer posts' ),
	            'screen_reader_text' => __( 'Post navigation' ),
	        )
	);
	the_post_navigation($args);
}


function bespoke_do_the_posts_navigation() {
	$args = apply_filters('bespoke_f_the_posts_navigation_args', array(
	            'prev_text'          => __( 'Older posts' ),
	            'next_text'          => __( 'Newer posts' ),
	            'screen_reader_text' => __( 'Posts navigation' ),
	        )
	);
	the_posts_navigation($args);
}

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function bespoke_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'bespoke_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'bespoke_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so bespoke_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so bespoke_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in bespoke_categorized_blog.
 */
function bespoke_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'bespoke_categories' );
}
add_action( 'edit_category', 'bespoke_category_transient_flusher' );
add_action( 'save_post',     'bespoke_category_transient_flusher' );

<?php

if (!defined('ABSPATH')) exit;

if (is_admin()){
	
	// set helpers (new)
	function da_set_option($field, $newvalue){
		if (empty($field))
			return false;
		$update_option = update_option(da_meta_field($field), $newvalue);
		return $newvalue;
	}

	function da_render_global_option_inputs(&$opts){
		$current_vals = array();
		$unique_concat = '';
		
		foreach ($opts as $id => $params){
			$current_vals[$id] = da_get_option($id);
			$unique_concat .= $id;
		}
		$nonce_field = 'da_render_global_option_inputs_'.md5($unique_concat."kmjvaFDbr%fkanh");
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			check_admin_referer($nonce_field);
			foreach ($opts as $id => $meta_box){
				$data = isset($_POST[$id.'_value']) ? $_POST[$id.'_value'] : '';
				if ($meta_box['input_type']=='date')
					$data = $data == '' ? '' : strtotime($data);
				if (is_array($data))
					$current_vals[$id] = da_set_option($id, array_map('stripslashes', $data));
				else
					$current_vals[$id] = da_set_option($id, stripslashes($data));
			}
		}
		
		echo '<form method="post" action="">';

		wp_nonce_field( $nonce_field );

		foreach($opts as $id=>$meta_box){
			$opts[$id]['__value'] = $current_vals[$id];
			$opts[$id]['__postid'] = null;
		}
		render_new_meta_boxes($opts);

		echo '<div class="bespoke_field"><p><input type="submit" class="button button-primary" name="save" /></p></div>';
		echo '</form>';	

	}

	// controller function
	function bespoke_global_settings(){
		if (!current_user_can('manage_options'))  
			wp_die( __('You do not have sufficient permissions to access this page.') );

		$opts = apply_filters('bespoke_f_global_settings', array());
		$name = apply_filters('bespoke_f_options_sitename', 'Bespoke Options');
		echo '<div class="bespokesettings"><h1>'.$name.' - Global Settings</h1>';
			da_render_global_option_inputs($opts);
		echo '</div>';	
	}

	add_action('admin_menu', 'bespoke_admin_menu');
	function bespoke_admin_menu(){
		$name = apply_filters('bespoke_f_options_sitename', 'Bespoke Options');
		$icon = apply_filters('bespoke_f_options_icon', get_template_directory_uri() . '/img/favicon.ico');
		add_menu_page('', $name, 'manage_options', 'bespoke', 'bespoke_global_settings', $icon);
		add_submenu_page('bespoke', 'General Settings', 'General Settings', 'manage_options', 'bespoke', 'bespoke_global_settings');
		
	}		




	add_action('admin_init', 'da_admin_init');				

	$__da_editor_styles = null;
	
	function da_get_style_array(){
		global $__da_editor_styles;
		if (is_null($__da_editor_styles))
			$__da_editor_styles = apply_filters('bespoke_f_editor_styles', array());	
		return $__da_editor_styles;
	}
	
	function da_admin_init(){
		global $bespoke;
		wp_register_style('bespoke_admin_style', get_template_directory_uri() . '/css/admin.css', array(), $bespoke->version());
		wp_enqueue_style('bespoke_admin_style'); 
		wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css'); 			
		$dependencies = apply_filters('bespoke_f_admin_js_dependencies', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'));
		wp_register_script('bespoke_admin_script', get_template_directory_uri(). '/js/bespoke_admin.min.js', $dependencies, $bespoke->version() );
		wp_enqueue_script('bespoke_admin_script'); 
		add_action('admin_footer', 'da_admin_inline_js', 99);
	}

	function da_admin_inline_js(){
		$options = array();
?>
		<script type='text/javascript'>
			//<![CDATA[
			da_admin.init(<?php echo json_encode(apply_filters('bespoke_f_admin_js_options', $options)); ?>)
			//]]>
		</script>
<?php
	}


	add_filter('mce_css', 'mcekit_editor_style');
	function mcekit_editor_style($url) {
		$editor_css = apply_filters('bespoke_f_editor_css_file', '');
		if (!empty($editor_css))
		{
			if ( !empty($url) )
				$url .= ',';
			$url .= trailingslashit( get_stylesheet_directory_uri() ) . $editor_css;
		}
		return $url;
	}

	add_filter('mce_buttons_2', 'mcekit_editor_buttons');
	function mcekit_editor_buttons($buttons) {
		// remove the font colour picker from TinyMCE
		if(($key = array_search('forecolor', $buttons)) !== false) {
			unset($buttons[$key]);
		}

		// add style selector
		$editor_styles = da_get_style_array();
		if (!empty($editor_styles))
		{
			array_unshift($buttons, 'styleselect');
		}
		return $buttons;
	}

	add_filter('tiny_mce_before_init', 'mcekit_editor_settings');
	function mcekit_editor_settings($settings){
		$editor_styles = da_get_style_array();
		if (empty($editor_styles))
			return $settings;

		if ( !empty($settings['theme_advanced_styles']) )
			$settings['theme_advanced_styles'] .= ';';
		else
			$settings['theme_advanced_styles'] = '';

		$class_settings = '';
		foreach ( $editor_styles as $name => $value ) {
			$class_settings .= "{$name}={$value};";
		}

		$settings['theme_advanced_styles'] .= trim($class_settings, '; ');

		return $settings;
	}

		// LEGACY bespoke_versions

	class da_bespoke_versions{
	
		private static $instance = null;
		private static $versions = array();

		public static function instance(){
			null === self::$instance AND self::$instance = new self;
			return self::$instance;
		}

		public function __construct(){
			self::$versions['4.00'] = array('New Major Version');
		}

		public function list_updates_check(){
			$isbespoketheme = (strpos(wp_get_theme()->Template, 'bespoke')!==False);
			if ($isbespoketheme) {
				$parent_theme_version = wp_get_theme(wp_get_theme()->Template)->Version;
			} else {
				$parent_theme_version = 'unknown';
			}
			printf('<h4>"%s" (child of %s %s)</h4>', esc_html(wp_get_theme()->Name), wp_get_theme()->Template, $parent_theme_version);
		}
	}

		// LEGACY bespoke_advanced_settings


		add_action('admin_menu', 'bespoke_advanced_menu', 99);
		function bespoke_advanced_menu(){
			add_submenu_page("bespoke", "Advanced", "Advanced Settings", 'manage_options', "da_advanced_options", "da_advanced_options"); 
		}

		function da_advanced_options(){
			if (!current_user_can('manage_options'))  
				wp_die( __('You do not have sufficient permissions to access this page.') );

			$fields = array(
				'head_scripts' => array(
						'name'			=> 'Header Scripts',
						'description'	=> 'Code added to all pages header area, including Google Analytics, webmasters access-codes etc. Must include open and close script tags.',
						'input_type'	=> 'textarea',
						'class'			=> 'large'
				),
				'foot_scripts' => array(
						'name'			=> 'Footer Scripts',
						'description'	=> 'Code added to all pages footer area. Must include open and close script tags.',
						'input_type'	=> 'textarea',
						'class'			=> 'large'
				)
			);
			
			$ver = da_bespoke_versions::instance();
			
			echo '<div class="themeinfo">';
				echo '<h1>Theme Info</h1>';
				$ver->list_updates_check();
			echo '</div>';
			
			echo '<div class="bespokesettings"><h1>Advanced Settings</h1>';
				da_render_global_option_inputs($fields);
			echo '</div>';
		}

}

add_action('bespoke_do_after_wp_head', 'da_wp_head_scripts', 99);  
function da_wp_head_scripts(){
	echo da_get_option('head_scripts');
}

add_action('bespoke_do_after_wp_footer', 'da_wp_foot_scripts'); 
function da_wp_foot_scripts(){
	echo da_get_option('foot_scripts');
}		



<?php

define('DA_WP_PRINT_STYLES_HOOK_PRIORITY', 8);

class da_bespoke_optimise{
	protected static $instance = null;
	private $css_entities = array();
	private $css_group = null;

	public static function instance(){
		null === self::$instance AND self::$instance = new self;
		return self::$instance;
	}

	function __construct(){
		add_action('admin_menu', array(&$this, 'menu'));
		add_action('init', array(&$this, 'init'));
	}

	function init(){
		if (da_get_option('enable_cdn') & 1){
			add_filter('wp_get_attachment_url', array(&$this, 'cdnify_url'), 99, 2);
			add_filter('stylesheet_directory_uri', array(&$this, 'cdnify_url'), 99, 3);
			add_filter('plugins_url', array(&$this, 'cdnify_url'), 99, 3);
			add_filter('script_loader_src', array(&$this, 'cdnify_url'), 99, 2);
		}
		
		//if (da_get_option('css_optimisation') & 1)
			//add_action('wp_head', array(&$this, 'setup_head_hooks'), DA_WP_PRINT_STYLES_HOOK_PRIORITY-1);
	}

	function cdnify_url($url, $dummy1 = 0, $dummy2 = 0){
		$url = preg_replace('/^'.preg_quote(get_bloginfo('url'),'/').'/', da_get_option('cdn_base_url'), $url);
		return $url;
	}
/*
	function setup_head_hooks()
	{
		$ret = remove_action('wp_head', 'wp_print_styles', DA_WP_PRINT_STYLES_HOOK_PRIORITY);
		if ($ret)
		{
			add_filter('style_loader_tag', array(&$this, 'handle_css_script_render'), 100, 2);
			add_action('wp_head', 'wp_print_styles', DA_WP_PRINT_STYLES_HOOK_PRIORITY);
			add_action('wp_head', array(&$this, 'print_concatenated_styles'), DA_WP_PRINT_STYLES_HOOK_PRIORITY);
		}
	}

	function handle_css_script_render($tag, $handle)
	{
		global $wp_styles;		
		$style = $wp_styles->registered[$handle];
		if ($style)
		{
			if ( isset($style->extra['conditional']) && $style->extra['conditional'] )
				return $tag;	// don't concat conditional scripts

			$src = $style->src;
			if ( ! strstr( $src, '?' ) ) 
				$src .= '?';
			list( $path, $query_string ) = explode( '?', str_replace( WP_CONTENT_URL, WP_CONTENT_DIR, $src ) );
			if (0===strpos($path, WP_CONTENT_DIR))
			{
				// only concat /wp-content/ scripts
				$this->css_entities[] = array(
					'path'=>$path,
					'style'=>$style,
					'tag'=>$tag
					);
				return '';			
			}
		}
		return $tag;
	}

	function print_concatenated_styles()
	{
		$groupUnique = '';
		foreach ($this->css_entities as $each) {
			$groupUnique.=$each['path'];
		}
		$groupName = 'c'.md5($groupUnique);

		print($groupUnique);
		print_r($this->css_entities);

		// output css file name

	}
*/
	function save_htaccess(){
		$gzip = da_get_option('enable_gzip_htaccess') == 1 ?
'<IfModule mod_deflate.c>
	AddOutputFilterByType DEFLATE text/html
	AddOutputFilterByType DEFLATE text/plain
	AddOutputFilterByType DEFLATE text/xml
	AddOutputFilterByType DEFLATE text/css
	AddOutputFilterByType DEFLATE text/javascript
	AddOutputFilterByType DEFLATE application/x-javascript
</IfModule>
' : '';

		$browser_cache = da_get_option('enable_browser_cache_htaccess') == 1 ?
'<IfModule mod_expires.c>
	ExpiresActive On
	<FilesMatch "\.(ico|jpg|jpeg|png|gif|js|css|swf)$">
		ExpiresDefault A2592000
	</FilesMatch>
</IfModule>
' : '';
		$access_origin = da_get_option('enable_cdn') == 1 ?
'<IfModule mod_headers.c>
    Header set Access-Control-Allow-Origin *
</IfModule>
' : '';
		$content = $gzip.$browser_cache.$access_origin;
		$home_path = get_home_path();
		$htaccess_file = $home_path.'.htaccess';
		$rules = explode( "\n", $content );
		insert_with_markers( $htaccess_file, 'DA_BESPOKE_OPTIMISE', $rules );
	}

	function menu(){
		add_submenu_page('bespoke', 'Optimise', 'Optimisation Settings', 'manage_options', 'da_optimise_options', array(&$this, 'options')); 
	}


	function options(){
		if (!current_user_can('manage_options'))  
			wp_die( __('You do not have sufficient permissions to access this page.') );

		$fields = array(
			'enable_gzip_htaccess' => array(
				'name'=>"Enable GZIP",
				'description'=>'Tick this box to enable compression of all html, CSS, JS and XML (if supported by the server) updates .htaccess file',
				'input_type'=>'checkbox'
				),
			'enable_browser_cache_htaccess' => array(
				'name'=>"Enable Browser Caching",
				'description'=>'Tick this box to enable the addition of hard browser caching (if supported by the server) updates .htaccess file',
				'input_type'=>'checkbox'
				),
			'enable_cdn' => array(
				'name'=>"Enable CDN",
				'description'=>'Tick this box to enable the CDN functionality',
				'input_type'=>'checkbox'
				),
			'cdn_base_url' => array(
				'name'=>"CDN Base url",
				'description'=>'The base url for the CDN assets. WARNING: Get this wrong and your site won\'t work!',
				'input_type'=>'input'
				)
		);
		echo '<div class="bespokesettings"><h1>Optimisation Settings</h1>';
			da_render_global_option_inputs($fields);
			if ($_SERVER['REQUEST_METHOD'] == 'POST') 
				$this->save_htaccess();
		echo '</div>';	
	}
}

da_bespoke_optimise::instance();
<?php

class da_bespoke_social {
	protected static $instance = null;
	private static $thisurl = null;
	private static $networks = array(
		'facebook'=>array(
			'name'=>'Facebook',
			'description'=>'The URL of your facebook page',
			),
		'twitter'=>array(
			'name'=>'Twitter @',
			'description'=>'Just your twitter name (without the @)',
			),
	);

	public static function instance(){
		null === self::$instance AND self::$instance = new self;
		return self::$instance;
	}


	protected function __construct(){
		add_action( 'init', array($this, 'init') );
		add_action('admin_menu', array(&$this, 'menu'));
	}

	public function init() {
		if (da_get_option('social_graph')) {
			add_action('bespoke_do_after_wp_head', array(&$this, 'graph_meta'));
		}
		self::$thisurl = getDomain() . add_query_arg(null,null);
		add_action('bespoke_do_social_shares', array(&$this, 'sharers_action'));
		add_action('bespoke_do_social_profiles', array(&$this, 'social_profiles_action'));
	}

	public function graph_meta() {
		$ogtitle =  get_bloginfo('name');
		if (!is_front_page())
			$ogtitle = wp_title('', false).' | '.$ogtitle;
		$ogtitle = apply_filters('bespoke_f_og_title', $ogtitle);
		$ogtype = apply_filters('bespoke_f_og_type', 'website');
		$ogdescription = apply_filters('bespoke_f_og_description', get_the_excerpt());

		$ogimage = '';
		$thumbId = get_post_thumbnail_id( );
		if ($thumbId) {
			$ogimage = wp_get_attachment_image_src( $thumbId, 'full' );
		}
		$ogimage = apply_filters('bespoke_f_og_image', $ogimage);
		if ($ogimage) {
			$ogimageurl = $ogimage[0];	
			$ogimagew = $ogimage[1];	
			$ogimageh = $ogimage[2];	
		}

		echo '<meta property="og:url" content="'.esc_attr(self::$thisurl).'"/>'."\n";
		echo '<meta property="og:type" content="'.$ogtype.'"/>'."\n";
		echo '<meta property="og:title" content="'.esc_attr(trim($ogtitle)).'"/>'."\n";
		if ($ogdescription) {
			echo '<meta property="og:description" content="'.esc_attr($ogdescription).'"/>'."\n";			
		}
		if ($ogimage) {
			echo '<meta property="og:image" content="'.esc_attr($ogimageurl).'"/>'."\n";
			echo '<meta property="og:image:width" content="'.esc_attr($ogimagew).'"/>'."\n";
			echo '<meta property="og:image:height" content="'.esc_attr($ogimageh).'"/>'."\n";
		}

		$twitter_ac = da_get_option('social_profile_twitter');
		if ($twitter_ac && $ogdescription) {
			echo '<meta name="twitter:card" content="summary">';
			echo '<meta name="twitter:site" content="@'.esc_attr(trim($twitter_ac)).'">';
			echo '<meta name="twitter:title" content="'.esc_attr(trim($ogtitle)).'">';
			echo '<meta name="twitter:description" content="'.esc_attr($ogdescription).'">';
			if ($ogimage) {
				echo '<meta name="twitter:image" content="'.esc_attr($ogimageurl).'">';
			}
		}

	}

	public function sharers($content) {
		$networks = da_get_option('social_share_networks');

		$content .= '<div class="social_shares">';

		if (in_array('facebook', $networks)) {
			$url = 'http://www.facebook.com/sharer/sharer.php?u=' . self::$thisurl;
			$content .=  '<div class="social_sharer facebook block"><i class="fa fa-facebook-official"></i> Share'; 
			$content .=  '<a class="cover" href="'.$url.'" target="_blank" rel="noopener" data-popup="640, 480" data-gaevent="sharepopup,facebook,'.esc_url(self::$thisurl).'"></a>';
			$content .=  '</div>';
		}

		if (in_array('twitter', $networks)) {
			$tweet_params = array('text'=>'Checkout this page: '.get_the_title(), 'url'=>self::$thisurl);
			$twitter_ac = da_get_option('social_profile_twitter');
			if ($twitter_ac) 
				$tweet_params['via'] = $twitter_ac;
			$url = 'https://twitter.com/intent/tweet?'. http_build_query($tweet_params);
			$content .=  '<div class="social_sharer twitter block"><i class="fa fa-twitter-square"></i> Tweet'; 
			$content .=  '<a class="cover " href="'.$url.'" target="_blank" rel="noopener" data-popup="640, 480" data-gaevent="sharepopup,twitter,'.esc_url(self::$thisurl).'"></a>';
			$content .=  '</div>';
		}
		$content .= '</div>';
		return $content;
	}

	public function sharers_action() {
		echo $this->sharers('');
	}

	public  function social_profiles_action() {
		$content = '';
		$profile = da_get_option('social_profile_facebook');
		if (!empty($profile)) {
			$url = esc_url($profile);
			$content .=  '<div class="social_profile facebook block"><i class="fa fa-facebook-square"></i>'; 
			$content .=  '<a class="cover " title="Like us on Facebook" href="'.$url.'" target="_blank" rel="noopener" data-gaevent="social,facebook,'.esc_url(self::$thisurl).'"></a>';
			$content .=  '</div>';			
		}

		$profile = da_get_option('social_profile_twitter');
		if (!empty($profile)) {
			$url = esc_url('https://twitter.com/intent/follow?screen_name='.$profile);
			$content .=  '<div class="social_profile twitter block"><i class="fa fa-twitter-square"></i>'; 
			$content .=  '<a class="cover " title="Follow us on twitter" href="'.$url.'" target="_blank" rel="noopener" data-popup="640, 480" data-gaevent="social,twitter,'.esc_url(self::$thisurl).'"></a>';
			$content .=  '</div>';			
		}

		$content = apply_filters('bespoke_f_social_profiles_content', $content);

		if (!empty($content)) {
			echo '<div class="social_profiles">'.$content.'</div>';
		}
	}

	public function menu(){
		add_submenu_page('bespoke', 'Social', 'Social Settings', 'manage_options', 'da_social_options', array(&$this, 'options')); 
	}

	public function options(){
		if (!current_user_can('manage_options'))  
			wp_die( __('You do not have sufficient permissions to access this page.') );

		$fields = array(
			'social_graph' => array(
				'name'=>"Enable Facebook Graph",
				'description'=>'Recommended - adds hidden data to pages which help with Facebook (and other) sharing',
				'input_type'=>'checkbox'
				),
			'social_share_networks' => array(
				'name'=>"Enable Share Buttons",
				'description'=>'Select networks for which share buttons are to be displayed',
				'input_type'=>'checkbox',
				'options'=>array_keys(self::$networks)
				),
		);
		foreach (self::$networks as $key => $value) {
			$fields['social_profile_'.$key] = array(
				'name'=>"Social Profile for ".$value['name'],
				'description'=>$value['description'],
				'input_type'=>'input'
				);
		}

		$fields = apply_filters('da_bespoke_f_social_admin_fields', $fields);
		
		echo '<div class="bespokesettings"><h1>Social Settings</h1>';
			da_render_global_option_inputs($fields);
		echo '</div>';	
	}

}

da_bespoke_social::instance();
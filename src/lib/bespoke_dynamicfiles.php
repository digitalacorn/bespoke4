<?php

	if (!defined('ABSPATH')) exit;

	add_action('wp_print_styles', 'da_dynamic_CSS_render', 1);
	// open head CSS
	function da_dynamic_CSS_render(){
		echo "<link rel='stylesheet' href='".get_site_url()."/bespoke/style.css' type='text/css' media='all' />\n";
	}

	add_action( 'init', 'da_php_to_css' );
	function da_php_to_css(){
		add_rewrite_rule(
			'^bespoke/([a-zA-Z0-9]+\.(png|css))/?$',
			'index.php?da_bespoke_filename=$matches[1]',
			'top' );
	}

	add_filter( 'query_vars', 'da_dynamic_files_query_vars' );
	function da_dynamic_files_query_vars( $query_vars ){
		$query_vars[] = 'da_bespoke_filename';
		return $query_vars;
	}		

	add_action( 'parse_request', 'da_dynamic_files_parse_request' );	
	function da_dynamic_files_parse_request( &$wp )
	{
		if ( array_key_exists( 'da_bespoke_filename', $wp->query_vars )) {
			if ('style.css'==$wp->query_vars['da_bespoke_filename']) {
				header("Content-type: text/css", true);
				header('Cache-control: public');
				header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 1209600) . ' GMT');		    	
				do_action('bespoke_do_dynamic_css');
				exit();		    		
			}
			else if ('responder.png'==$wp->query_vars['da_bespoke_filename']) {
				if (isset($_GET['width']) && isset($_GET['height']) && isset($_GET['verify'])) {
					$w = $_GET['width'];
					$h = $_GET['height'];						
					$v = $_GET['verify'];
					if ((!$w || !$h) || (!is_numeric($w) || !is_numeric($h)) || ($w > 3000 || $h > 3000)) {
						header("HTTP/1.0 404 Not Found");
						exit();							
					}
					if ($v!=DAhash($w.'-'.$h, 'responder_image')) {
						header("HTTP/1.0 404 Not Found");
						exit();
					}
					$img = imagecreatetruecolor($w, $h);
					imagesavealpha($img , true);			
					imagefill($img, 0, 0, 0x7fff0000);				
					header('Content-Type: image/png');
					header('Cache-control: public');
					header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 1209600) . ' GMT');		    	
					imagepng($img);
					imagedestroy($img);	
					exit();						
				}
			} 
			else
			{
				header("HTTP/1.0 404 Not Found");
				require TEMPLATEPATH.'/404.php';
				exit();		    			
			}
		}
	}

		

<?php

	if (!defined('ABSPATH')) exit;

	// CREATE USER INPUTS

	/*
		Filters:
		bespoke_meta_boxes : filters add/remove entries and adjust text, post_types, seo adds meta fields to all post_types by default
		bespoke_supported_post_types : add or remove supported post-types, no Bespoke Meta options will appear if type not in this list
	*/
	
// 				"meta_keywords" => array(		// array key is the ID and database name - no spaces allowed
// 					"default" => "",			// the default value, to insert into the field when empty
// 					"post_types" => NULL,			// null means all suppported post_types, otherwise supply and array of post types
// 					"exclude_post_types" => NULL,	// if set means all post types except the ones in the array
// 					"input_type" => "input",	// supported types: input, wysiwyg, textarea, checkbox, date, custom
// 					"name" => "SEO Keywords",	// the display name
// 					"description" => "info",	// more info about the field to help users
//					"handler" => "classname"	// (for custom type) class to instantiate and call render, or save methods
//					"columns" => "function or BOOL",	// whether a column should appear in lists
//					"priority" => (int), 		// order the field appears. 
//					"sortable" => "function or BOOL")	// if it is sortable or not, 'num' will do a numeric sort on value, or call your own method

		class DAMetaInfoHolder {
			/* class used to wrap the current meta-info array element, so it can be passed to filters and actions used for columns and sorting
			*/
			public function __construct($id, $meta) {
				$this->id = $id;
				$this->meta = $meta;
			}
			public function column_edit($columns) {
				if (isset($columns['date'])) {	
					$date = $columns['date'];
					unset($columns['date']);
				}
				$new_columns[$this->id] = apply_filters('bespoke_f_meta_admin_column_'.$this->id.'_title', $this->meta['name']);
				if (isset($date)) {
					$new_columns['date'] = $date;
				}
				return array_merge($columns, $new_columns);	
			}
			public function column_content($column_name, $id){
				global $post;
				$this->post = &$post;
				switch ($column_name) {
					case $this->id:
						$metaval = da_get_post_meta($post->ID , $this->id);
						if ($this->meta['columns']===true) {
							switch ($this->meta['input_type']) {
								case 'select':
									if (isset($this->meta['options'][$metaval]))
										echo $this->meta['options'][$metaval];
									break;
								default:
									echo $metaval;
									break;
							}
						}
						else {
							echo $this->meta['columns']($this, $metaval);
						}
						break;
					default:
						break;
				}
			}
			public function column_sortable($columns) {
				$columns[$this->id] = $this->id;
				return $columns;
			}
			public function column_orderby( $vars ) {
				if ( isset( $vars['orderby'] ) && $this->id == $vars['orderby'] ) {
					if ($this->meta['sortable']===true) {
						$quargs = array(
							'orderby' => 'meta_value',
							'meta_key' => da_meta_field($this->id)
						);
					}
					elseif ($this->meta['sortable']==='num') {
						$quargs = array(
							'orderby' => 'meta_value_num',
							'meta_key' => da_meta_field($this->id)
						);
					}
					else {
						$quargs = $this->meta['sortable']($this);
					}
					$vars = array_merge( $vars, $quargs );
				}
				return $vars;
			}
		}

		$__da_metaboxes = null;
		function GetMetaBoxes(){
			global $__da_metaboxes;
			if (is_null($__da_metaboxes))
				$__da_metaboxes = apply_filters('bespoke_f_meta_fields', array());
			return $__da_metaboxes;
		}

		function DA_post_types_for_meta($field_def){	
			$excludes = isset($field_def['exclude_post_types']) && is_array($field_def['exclude_post_types']);
			if ($excludes || is_null($field_def['post_types'])){
				$args=array(
				  'public'   => true,
				  '_builtin' => false
				); 
				$all_post_types = array_values(get_post_types($args,'names'));
				$all_post_types[] = 'page';
				$all_post_types[] = 'post';	
				if ($excludes)
				{
					return array_diff($all_post_types, $field_def['exclude_post_types']);	
				}
				return $all_post_types;
			}
			if (is_array($field_def['post_types']))
				return $field_def['post_types'];
			else
				return array($field_def['post_types']);
		}

		function DA_SortMetaBoxes($a, $b) 
		{
			$cmp = $a['priority'] - $b['priority'];
			return $cmp ? $cmp : $a['__native_order'] - $b['__native_order'];
		}

		function DA_field_visibility($id, $meta_box)
		{
			$vis = in_array(get_post_type(),DA_post_types_for_meta($meta_box));
			return apply_filters('bespoke_f_meta_field_visibility', $vis, $id);
		}

		function new_meta_boxes() 
		{
			global $post;
			$pmb = GetMetaBoxes();
			$filtered = array();
			foreach($pmb as $id=>$meta_box){
				if (!DA_field_visibility($id, $meta_box)){
					continue;
				}
				$filtered[$id] = $meta_box;
				$filtered[$id]['__value'] = da_get_post_meta($post->ID, $id);
				$filtered[$id]['__postid'] = $post->ID;
			}
			render_new_meta_boxes($filtered);
		}

		function render_new_meta_boxes($pmb) 
		{

			$first = "first";
			$wysiwyg_id = "bespokewysiwyg";
			$wysiwyg_count = 1;

			// Sort the fields
			$count = 0;
			foreach($pmb as $id=>&$meta_box2) 
			{
				$defaults = array('class' => null, 'options'=>array(), 'priority'=>10, '__native_order'=>$count++);
				$meta_box2 = array_merge($defaults, $meta_box2);
			}
			uasort($pmb, 'DA_SortMetaBoxes');

			echo'<input type="hidden" name="bespoke_meta_noncename" id="bespoke_meta_noncename" value="'.wp_create_nonce( plugin_basename(__FILE__) ).'" />';


			foreach($pmb as $id=>$meta_box){
				$class = $meta_box['class'];

				$meta_box_value = $meta_box['__value'];

				$inputextra = '';
				if (isset($meta_box['readonly']))
					$inputextra .= $meta_box['readonly'] ? ' readonly ' : '';
				 
				if(isset($meta_box['default']) && $meta_box_value == '')
					$meta_box_value = $meta_box['default'];


				printf('<div class="bespoke_field '.$class.' '.$first.' '.$meta_box['input_type'].'"><label for="%s_value">%s</label>', $id, $meta_box['name']);

				if($meta_box['input_type'] == 'date'){
					// convert timestamp for date into string for datepicker
					$meta_box_value = $meta_box_value=="today" ? time() : $meta_box_value;
					$meta_box_value = $meta_box_value=="" ? "" : date(BESPOKE_DATE_FORMAT, $meta_box_value);
				}
				
				if($meta_box['input_type']=='textarea')
					echo'<textarea class="bespoke_input" name="'.$id.'_value" cols="80" rows="3" '.$inputextra.'>'.esc_textarea($meta_box_value).'</textarea>';
				elseif($meta_box['input_type'] == 'checkbox'){						
					if (!empty($meta_box['options'])){
						$default_value = !empty($meta_box['default']) ? $meta_box['default'] : '';
						
						foreach ($meta_box['options'] as $key => $value){
							$checked = '';
							if (is_array($meta_box_value) && in_array($value, $meta_box_value))
								$checked = ' checked="checked"';
							elseif ($default_value == $value)
								$checked = ' checked="checked"';
							echo '<input type="' . $meta_box['input_type'] . '" name="' . $id . '_value[]" value="' . $value . '"' . $checked . '/><span class="checkbox_label">' . esc_html($value) . '</span>';
						}
					}
					else{
						$checked = $meta_box_value=="1" ? "checked='checked'" : "";
						echo '<input type="checkbox" class="bespoke_input" name="'.$id.'_value" value="1" '. $checked . ' />';
					}
				}
				elseif($meta_box['input_type']=='radio'){
					foreach ($meta_box['options'] as $key => $value){
						$checked = $meta_box_value == $key ? 'checked="checked"' : '';
						echo '<input type="radio" name="'.$id.'_value" value="' . $key . '" ' . $checked . '><label class="inner">' . $value . '</label>';
					}
				}				
				elseif($meta_box['input_type']=='pageselect'){
					$page_dropdown__defaults = array( 
						'show_option_none' => 'Select page...', 
					);
					$page_select_options = array_merge($page_dropdown__defaults, $meta_box['options']);
					$page_select_options['name'] = $id.'_value';
					$page_select_options['selected'] = $meta_box_value;
					wp_dropdown_pages($page_select_options);
				}
				elseif($meta_box['input_type']=='select'){
					$checked = $meta_box_value=="1" ? "checked='checked'" : "";
					echo '<select class="bespoke_input" name="'.$id.'_value">';
					foreach ($meta_box['options'] as $key => $value){
						$selected = $meta_box_value==$key ? "selected='selected'" : "";
						echo '<option value="'.$key.'" '.$selected.'>'.esc_html($value).'</option>';
					}
					echo '</select>';
				}

				elseif($meta_box['input_type']=='document'){
					echo '<input type="hidden" class="bespoke_input" name="'.$id.'_value" value="'.esc_attr($meta_box_value).'"/>';
					echo '<div class="actions"><div class="select button">Select Document</div> <div class="clear button">Clear</div></div>';
					echo '<div id="' . get_the_id() . '" class="thumbs"></div>';
				}
				elseif($meta_box['input_type']=='media'){
					echo '<input type="hidden" class="bespoke_input" name="'.$id.'_value" value="'.esc_attr($meta_box_value).'"/>';
					echo '<div class="actions"><div class="select button">Select Media</div> <div class="clear button">Clear</div></div>';
					echo '<div id="' . get_the_id() . '" class="thumbs"></div>';
				}
				elseif($meta_box['input_type']=='wysiwyg'){
					$editor_options_defaults = array( 
										'textarea_rows' => 5, 
										'media_buttons' => false
										);
					$editor_options = array_merge($editor_options_defaults, $meta_box['options']);
					$editor_options['textarea_name'] = $id.'_value';
					echo '<div class="bespoke_input" >';
					wp_editor( $meta_box_value, $wysiwyg_id.$wysiwyg_count, $settings = $editor_options );
					echo '</div>';
					$wysiwyg_count += 1;
				}
				elseif($meta_box['input_type']=='custom'){
					$handler = new $meta_box['handler'];
					$mh = new DAMetaInfoHolder($id,$meta_box);
					$handler->render($meta_box['__postid'], $mh);
				}
				else
					echo'<input type="text" class="bespoke_input" name="'.$id.'_value" value="'.esc_attr($meta_box_value).'" size="55" '.$inputextra.'/>';
				
				$meta_box_description = isset($meta_box['description']) ? $meta_box['description'] : '';
				echo '<div class="description">' . $meta_box_description . '</div></div>';
				$first = '';
			}
		}

		function create_meta_box(){
			if (function_exists('add_meta_box')){
				$pmb = GetMetaBoxes();
				$used_types = array();
				foreach ($pmb as $field) {
					$used_types = array_unique(array_merge($used_types,DA_post_types_for_meta($field)));
				}
				$sitename = apply_filters('bespoke_f_options_sitename', "Bespoke Options");
				foreach ($used_types as $post_type ) 
				{
					$pt = get_post_type_object($post_type);
					if (isset($pt))
						add_meta_box( 'DigitalAcorn_Bespoke_post_meta', 'Bespoke Options for '.$sitename.' - '.$pt->labels->name, 'new_meta_boxes', $post_type, 'normal', 'high' );
				}
			}
		}

		function da_set_post_meta( $post_id = null, $field, $value ) { 
			if ( is_null( $post_id ) ){
				global $post;
				if ( isset( $post ) )
					$post_id = $post->ID;
				else
					return;
			}
			$pm = da_get_post_meta( $post_id, $field );
			$key = da_meta_field( $field );
			if ( $value == '' )
				delete_post_meta( $post_id, $key) ;
			elseif( $pm === false ){
				add_post_meta( $post_id, $key, $value, true );
			}
			elseif( $value != $pm )
				update_post_meta( $post_id, $key, $value );
		}

		function save_postdata( $post_id ) 
		{
			$pmb = GetMetaBoxes();

			if ( !isset( $_POST['bespoke_meta_noncename']) || !wp_verify_nonce( $_POST['bespoke_meta_noncename'], plugin_basename(__FILE__) )) 
			{
				return $post_id;
			}

			foreach($pmb as $id=>$meta_box) 
			{
				if (DA_field_visibility($id,$meta_box))
				{
				// Verify
					 
					if ( 'page' == $_POST['post_type'] && isset($_POST['post_type'])) 
					{
						if ( !current_user_can( 'edit_page', $post_id ))
							return $post_id;
					} 
					else 
					{
						if ( !current_user_can( 'edit_post', $post_id ))
							return $post_id;
					}

					if($meta_box['input_type'] == 'custom'){
						$handler = new $meta_box['handler'];
						$mh = new DAMetaInfoHolder($id,$meta_box);
						$handler->save($post_id, $mh);
						continue;
					}
				 
					$data = isset($_POST[$id.'_value']) ? $_POST[$id.'_value'] : '';

					if ($meta_box['input_type']=='date')
						$data = $data == '' ? '' : strtotime($data);

					da_set_post_meta( $post_id,  $id, $data );

				}
			}
		}

		function post_meta_admin_init($post_id){
			$pmb = GetMetaBoxes();
			foreach($pmb as $id=>$meta_box){
				$mh = new DAMetaInfoHolder($id,$meta_box);
				if (isset($meta_box['columns'])){
					$post_types = DA_post_types_for_meta($meta_box);
					foreach ($post_types as $p) {
						add_filter('manage_edit-'.$p.'_columns', array($mh, 'column_edit'), 10, 1);
						add_action('manage_'.$p.'_posts_custom_column', array($mh, 'column_content'), 10, 2);
						//http://wordpress.org/support/topic/admin-column-sorting
						if (isset($meta_box['sortable']) && $meta_box['sortable']){
							add_filter('manage_edit-'.$p.'_sortable_columns', array($mh, 'column_sortable'), 10, 1);
							add_filter( 'request', array($mh, 'column_orderby'));
						}

					}
				}
					
			}
		}

		add_action('print_media_templates', 'da_bespoke_media_meta_template');
		function da_bespoke_media_meta_template() {
			?>
<script type="text/html" id="tmpl-da-bespoke-media-meta">
		<div class="thumbnail">
				<# if ( 'image' === data.type && data.sizes ) { #>
					<img src="{{ data.sizes['thumbnail'].url }}" alt="" />
					<div class="filename">
						{{ data.title }}
					</div>
				<# } else { #>
					<# if ( data.image && data.image.src && data.image.src !== data.icon ) { #>
						<img src="{{ data.image.src }}" class="thumbnail" />
					<# } else { #>
						<img src="{{ data.icon }}" class="icon" />
					<# } #>
					<div class="filename">
						{{ data.filename }}
					</div>
			<# } #>
		</div>
</script>
			<?php 
	}

		function bespoke_meta_thumbs(){
			if ( ! current_user_can( 'upload_files' ) || !isset($_GET['ids']) || empty($_GET['ids']))
				wp_send_json_error();
			$ids = explode(',', $_GET['ids']);
			$id_count = count($ids);
			$query = array('post_type'=>'attachment', 'post_status'=>'inherit', 'posts_per_page'=>-1, 'orderby'=>'post__in', 'post__in'=>array() );
			foreach ($ids as $id) {
				if (is_numeric($id) && $id>0)
					$query['post__in'][] = $id;
			}
			$metafield = preg_replace('/_value$/', '', $_GET['field']);
			$pmb = GetMetaBoxes();
			if (!array_key_exists($metafield, $pmb) || !in_array($pmb[$metafield]['input_type'], array('media','document'))) {
				wp_send_json_error($pmb );
				return;
			}

			$query = new WP_Query( $query );
			if (!empty($query) && !is_wp_error($query) && isset($query->posts)){
				$post_count = count($query->posts);
				if ($id_count > $post_count && isset($_GET['pid'])){
					$actual_ids = '';
					$x = 1;
					foreach($query->posts as $p){
						if ($x != 1)
							$actual_ids .= ',';
						$actual_ids .= $p->ID;
						$x++;
					}
					update_post_meta($_GET['pid'], da_meta_field($metafield), $actual_ids);
				}
			}
			$posts = array_map( 'wp_prepare_attachment_for_js', $query->posts );
			$posts = array_filter( $posts );
			wp_send_json_success( $posts );
			die();
		}

		add_action('wp_ajax_bespoke_meta_thumbs', 'bespoke_meta_thumbs');

		add_action('admin_init', 'post_meta_admin_init');
		add_action('add_meta_boxes', 'create_meta_box');
		add_action('save_post', 'save_postdata');
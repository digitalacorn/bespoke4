<?php

class da_bespoke_fontawesome {
	protected static $instance = null;

	public static function instance(){
		null === self::$instance AND self::$instance = new self;
		return self::$instance;
	}

	protected function __construct(){
		add_action( 'wp_enqueue_scripts', array($this, 'scripts') );
	}

	public function scripts() {
		global $bespoke;
		wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css', array(), $bespoke->version('parent') );
	}
}

da_bespoke_fontawesome::instance();
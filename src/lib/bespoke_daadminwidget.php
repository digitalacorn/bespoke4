<?php

	if (!defined('ABSPATH')) exit;

		/* fetches feeds from digital acorn onto dashboard */
		define("FORCE_UPDATE", false);
		define("SEOVERSION", "1.1");

		add_action('admin_init', 'digitalacorn_admin_init');
		function digitalacorn_admin_init(){
			add_action('wp_ajax_dashboard-widgets', 'da_ajax_dash_widget', 1);
		}

		function da_ajax_dash_widget(){
			require ABSPATH . 'wp-admin/includes/dashboard.php';
			if ($_GET['widget'] == 'digitalacorn_dashboard_widget')
				digitalacorn_dashboard_widget_function();
			wp_die();
		}

		function wp_da_blog_output($id, $feeds){
			$parent_dir = wp_get_theme()->get( 'Template' );
			$my_theme = wp_get_theme($parent_dir);
			$version = $my_theme->exists() ? $my_theme->get('Version') : SEOVERSION;
			 
			echo '<div class="rss-widget">';
			$url = 'http://api.digitalacorn.co.uk/wordpress/wp_admin_notice.php?'.http_build_query(array(
					'site'=>get_bloginfo('url'),
					'version'=>$GLOBALS['wp_version'],
					'bespoke'=>$version
				),'','&' );  

			if (function_exists("curl_init"))
			{
				$ch = curl_init();
				curl_setopt( $ch, CURLOPT_URL, $url );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
				$content = curl_exec( $ch );
				$response = curl_getinfo( $ch );
				curl_close( $ch );
				$valid = ($response['http_code'] == 200);
			}
			else
			{
				$content=file_get_contents($url);
				$valid = (FALSE !== $content);
			}

			if ($valid)
			{
				$data = json_decode($content);
				if (!is_null($data) && $data->message!=="")
				{
					echo '<div class="da_notice '.$data->level.'">';
					echo '<span class="reload_wrap"><a class="reload" href="#"></a></span>';
					echo $data->message;
					echo '</div>';
				}
			}

			echo '<h4><strong>Help and Tips</strong></h4>';
			wp_widget_rss_output($feeds[0], array('items'=>2, 'show_summary'=>1));
			echo '<h4><strong>The Lastest From Our Blog</strong></h4>';
			wp_widget_rss_output($feeds[1], array('items'=>2, 'show_summary'=>1));
			echo "</div>";
		}

		function digitalacorn_dashboard_widget_function() {
			$da_tips_cat_id = 84;
			$feeds = array(
						"http://www.digitalacorn.co.uk/?cat=" . $da_tips_cat_id . "&feed=rss2",
						"http://www.digitalacorn.co.uk/?cat=-" . $da_tips_cat_id . "&feed=rss2"
					);
							
			if (FORCE_UPDATE)
				delete_transient('dash_' . md5( "digitalacorn_dashboard_widget" ));	
				
			wp_dashboard_cached_rss_widget( 'digitalacorn_dashboard_widget', 'wp_da_blog_output', $feeds );
		} 

		add_action('wp_dashboard_setup', 'bespoke_digitalacorn_dash_hooks', 1);
		function bespoke_digitalacorn_dash_hooks(){
			if( current_user_can( 'administrator' ) )	
				add_meta_box( 'digitalacorn_dashboard_widget', 'Digital Acorn Limited', 'digitalacorn_dashboard_widget_function', 'dashboard', 'default', 'high' );
		}

		add_action('wp_ajax_bespoke_digitalacorn_dash_invalidate', 'bespoke_digitalacorn_dash_invalidate');
		function bespoke_digitalacorn_dash_invalidate(){
			delete_transient('dash_' . md5( "digitalacorn_dashboard_widget" ));		
			die();
		}		


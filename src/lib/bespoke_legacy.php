<?php

	if (!defined('ABSPATH')) exit;


		// LEGACY bespoke_helpers
			
		function da_bespoke_page_for_archive(){
			// finds a matching page via slug (from deepest path, upwards until a match is found) 
			// returns null if no match
			global $wp;	
			if (!is_archive())
				return null;
			$path = $wp->request;
			while (true)
			{
				$query = new WP_Query( 'pagename='.$path );
				if(isset($query->posts) && !empty($query->posts)){
					$page = current($query->posts);
					if ($page)
						return $page;
				}
				$path = explode('/', $path);
				array_pop($path);
				if (count($path)<=0)
					return null;
				$path = implode('/', $path);
			}
		}

		function da_list_hooks(){
			global $wp_filter;
			$hook=$wp_filter;
			ksort($hook);
			echo '<pre>';
			foreach($hook as $tag => $priority){
				echo "<br/>&gt;&gt;&gt;&gt;&gt; <strong>$tag</strong><br/>";
				ksort($priority);
				foreach($priority as $priority => $function)
				{
					foreach($function as $name => $properties) 
						echo "$priority $name<br/>";
				}
			}
			echo '</pre>';
			return;
		}
		
		// check if global post var is set and return id if it is.  
		function da_check_post_return_id(){
			global $post;			
			if (isset($post))
				return $id = $post->ID;
			else
				return false;
		}

		// get all post attachments (images) excluding the featured image
		function get_my_images_exclude_featured($post_id = false){
			if ($post_id === false)
				$this_post = get_the_ID();
			else
				$this_post = $post_id;
			
			$featured_img_id = get_post_thumbnail_id( $this_post );
			
			$images = get_children("exclude=" . $featured_img_id . "&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&post_parent=" . $this_post );
			
			return $images;
		}	

		// get all post attachments (images)
		function get_my_images($post_id = false, $meta=false){			
			if ($post_id === false)
				$this_post = get_the_ID();
			else
				$this_post = $post_id;

			if ($meta) {
				$ids = da_get_post_meta($this_post, $meta);	
				$ids = $ids ? array_map('intval', explode(',', $ids)) : array();
				$images = empty($ids) ? array() : get_posts(array('posts_per_page'=>-1, 'post_type'=>'attachment', 'post__in' => $ids, 'post_status' => null, 'post_mime_type' => 'image', 'orderby'=>'post__in'));
			} else {
				$images = get_children("post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&post_parent=" . $this_post );
			}
						
			return $images;
		}	
		
		// image ID's for a given post
		function da_image_ids($post_id = false, $meta=false){
			$ids = null;
			if ($post_id === false)
				$this_post = get_the_ID();
			else
				$this_post = $post_id;	
			if ($meta) {
				$ids = da_get_post_meta($this_post, $meta);
				$ids = $ids ? array_map('intval', explode(',', $ids)) : array();
				$images = empty($ids) ? array() : get_posts(array('posts_per_page'=>-1, 'post_type'=>'attachment', 'post__in' => $ids, 'post_status' => null, 'post_mime_type' => 'image', 'orderby'=>'post__in', 'fields'=>'ids'));
			}
			else {
				$ids = get_children("post_type=attachment&orderby=menu_order&order=ASC&fields=ids&post_mime_type=image&post_parent=" . $this_post );
			}
			return $ids;
		}
		
		// display post attachments
		function display_my_images($id, $cust_size, $echo = true){
			if (is_array($cust_size))
				$cust_size_class = 'user_defined';
			else
				$cust_size_class = $cust_size;
				
			$this_image = wp_get_attachment_image($id, $cust_size, 0, array('alt' => trim(strip_tags(get_post_meta($id, '_wp_attachment_image_alt', true))), 'class' => apply_filters('da_slideshow_image_class', "attachment-" . $cust_size_class)));
			
			if ($echo)
				echo $this_image;
			else
				return $this_image;
		}
		
		// get post attachments array url[0] / width[1] / height[2] 
		function get_my_image_url($id, $cust_size, $attachment = false) {				
			$this_image = wp_get_attachment_image_src($id, $cust_size, $attachment);
			return $this_image;
		}

		// get post attachment meta 
		function get_my_image_meta($id = 1){							
			$attachment = get_post( $id );
			return array(
				'caption' => $attachment->post_excerpt,
				'description' => $attachment->post_content,
				'title' => $attachment->post_title
			);
		}

		// columns 
		function DA_multicolumn_shortcode($atts, $content=null){
			$defaults = array('num' => 2);
			extract( shortcode_atts( apply_filters('da_columns_defaults', $defaults), $atts ), EXTR_SKIP );
			$num = intval($num);
			return '<div class="da_columns maxcolumns'.$num.'">' . $content . '</div>';
		}
			
		class PostImages{
			protected static $instance = null;
			private $usedImages;
			private $counter;
			private $images;
			private $imageIds;
			
			public static function instance(){
				null === self::$instance AND self::$instance = new self;
				return self::$instance;
			}
			
			private function __construct(){
				global $post;
				$this->images = get_my_images($post->ID);
				$this->imageIds = array_keys($this->images);
				$this->featured = get_post_thumbnail_id($post->ID);
				$this->featuredPos = array_search($this->featured, $this->imageIds);
				$this->usedImages = array();
				$this->counter = 0;
			}
			
			public function isUsed($imageNum) {
				return array_key_exists($imageNum, $this->usedImages);
			}
			
			public function render($imageNum, &$atts) {
				if ($atts['featured'])
					$attId = $this->featured;
				else
					$attId = $this->getImageAttatchmentId($imageNum);
				if (empty($attId))
					return;
				$this_image = apply_filters('da_image_list_container_open', '<div class="da_image_list">');
				$size = apply_filters('da_image_list_image_size', 'full_img');
				if ($atts['cover']){
					$thumb = wp_get_attachment_image_src($attId, $size);
					$this_image .= '<div style="background: url(' . $thumb[0] . ') no-repeat 0 0; background-size: cover;">'.da_responder_image(1200,900).'</div>';
				}
				else {
					$this_image .= display_my_images($attId, $size, false);
				}
				if ($atts['caption']){
					$meta = get_my_image_meta($attId);
					if (isset($meta['caption']))
						$this_image .= apply_filters('da_image_list_caption', '<div class="da_image_caption">' . $meta['caption'] . '</div>');
				}
				$this_image .= apply_filters('da_image_list_container_close', '</div>');
				return $this_image;
			}
			
			public function getImageAttatchmentId($imageNum, $andUse=true) {
				// flags the image as used
				$imageNum = intval($imageNum);
				if ($imageNum>=count($this->imageIds) || $imageNum<0)
					return null;
				if ($andUse)
					$this->usedImages[$imageNum] = true;
				return $this->imageIds[$imageNum];
			}
			
			public function useFeatured() {
				return false===$this->featuredPos ? null : $this->getImageAttatchmentId($this->featuredPos);
			}
			
			public function remainingImages($andUse=true) {
				$ret = array();
				foreach ($this->imageIds as $key => $value) {
					if (!$this->isUsed($key))
						$ret[] = $this->images[$this->getImageAttatchmentId($key,$andUse)];
				}
				return $ret;
			}
			
			public function nextUnusedImage(&$atts) {
				while ($this->isUsed($this->counter))
					$this->counter++;
				return $this->render($this->counter, $atts);
			}
			
			public static function shortcode($atts) {
				$inst = self::instance();
				$atts = shortcode_atts(array(
					'featured' 	=> false,
					'skip'	 	=> false,
        			'cover' 	=> false,
					'caption' 	=> false,
        			'num' 		=> null,
    			), $atts);
				if (!is_null($atts['num']))
					return $inst->render($atts['num'], $atts);
				else if ($atts['skip'])
					$inst->counter+=intval($atts['skip']);
				else if ($atts['featured'])
					return $inst->render(null, $atts);
				else 
					return $inst->nextUnusedImage($atts);				
			}
		}
		
		// better read more by default
		function da_new_excerpt_more(){
			global $post;
			return '... <a class="moretag" href="'. get_permalink($post->ID) . '">' . apply_filters('da_read_more_text', 'read more') . '</a>';
		}	

		// custom titles 
		function da_custom_post_title($og_title){	
			global $post;
			$title = str_replace('&', '&amp;', do_shortcode(da_get_post_meta($post->ID, 'page_title')));
			if (empty($title))
				$title = $og_title;
			return $title;
		}	
		
		// fully remove titles
		function da_remove_post_title($og_title){	
			global $post;
			if ($a = da_get_post_meta($post->ID, 'remove_title'))
				return false;
			else
				return $og_title;
		}
		
		// do slideshow	
		function DA_slideshow($post_id = false) {
			
			global $post;
			
			if ($post_id)
				$id = $post_id;
			elseif (isset($post))
				$id = $post->ID;
			else
				return false;
			
			$thumbs = '';
			$class = '';
			$sub_arrows = '';
			$images = get_my_images($id);
			if (!empty($images)){				
				$nav = '';
				$css = '';
				$slides = '';
				$image_meta = '';
				$x = 1;
				$y = 1;
				$count = count($images);
				if ($count > 5)
					$sub_arrows = '<div class="nav_block_nav prev_block"></div><div class="nav_block_nav next_block"></div>';
				else
					$class = ' no_sub_nav';				
				$nav .= '<div class="nav_group" id="nav_group_' . $y .'">' . $sub_arrows;
				foreach ($images as $image)
				{
					$url = get_my_image_url($image->ID, apply_filters('da_slideshow_image_size', 'main_images'), false);
					
					$fifth = '';
					
					if ($x % 5 === 0)
						$fifth = ' fifth';

					if ($count > 1)
						$nav .= '<div class="slide_nav_item' . $fifth . '" id="img_' . $x . '">' . display_my_images($image->ID, array(96, 96), false) . '</div>';

					if ($x % 5 === 0)
					{
						$y++;
						$nav .= '</div><div class="nav_group" id="nav_group_' . $y .'">' . $sub_arrows;
					}	
					$slides .= '<div class="slideshow_img" id="slideshow_img_' . $x . '"></div>';
					$css .= '#slideshow_img_' . $x . ' {background: url(' . $url[0] . ') no-repeat scroll 50% 50%;}';
					
					
					$x++;
				}
				
				$nav .= '</div>';

				$style = '<style type="text/css">' . $css . ' #da_slideshow .slideshow_img {background-size: cover;}</style>';
				$slideshow = '<div id="da_internal_slideshow">' . $slides . '</div>';
				$navigation = '<div class="slide_navigation"><div class="slide_arrows previous_arrow"></div><div class="slide_arrows next_arrow"></div></div>';
				$thumbs = '<div id="da_slideshow_nav"><div class="da_slideshow_nav">' . $nav . '</div></div>';
				
				return $style . '<div id="internal_slideshow_wrap" class="unselectable ' . $class . '">' . $navigation . $slideshow . $thumbs . '</div>';
			}
			
			return false;
		}
		
		function da_image_if_avail($index, $images, $img_size = 'full_img', $count = false){
			$count = $count ? $count : count($images);
			if ($index <= $count)
				return display_my_images($images[$index-1]->ID, $img_size, false);
			else
				return '';
		}

		function da_featuredtoRSS($content){
			global $post;
			if (has_post_thumbnail($post->ID))
				$content = apply_filters('da_featuredtoRSS_image_wrap_open', '') . get_the_post_thumbnail($post->ID, 'thumbnail', array('style' => 'float: left; margin: 0 15px 15px 0;')) . apply_filters('da_featuredtoRSS_image_wrap_close', '') . $content;
			return $content;
		}
		
		function da_get_file_contents($file){
			if (function_exists('curl_version')){
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $file);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$content = curl_exec($curl);
				curl_close($curl);
			}
			else
				$content = file_get_contents($file);
			
			return $content;
		}

		// LEGACY bespoke_help


		add_action('admin_menu', 'da_bespoke_help_init', 99);

		global $help_page_content;

		function da_bespoke_help_init(){
			global $help_page_content;
			$help_page_content = apply_filters('da_help_page_content', '');
			if ($help_page_content!='')
				add_submenu_page('bespoke', 'Help', 'Help Infomation', 'manage_options', 'bespoke_admin_help_page', 'bespoke_admin_help_page'); 
		}

		function bespoke_admin_help_page(){
			if (!current_user_can('manage_options'))
				wp_die( __('You do not have sufficient permissions to access this page.') );
			
			global $help_page_content;
			echo '<div class="da_help_content">' . $help_page_content;
				echo '<h5> &copy; 2013-' . date('Y') .' <a href="http://www.digitalacorn.co.uk/" rel="noopener" target="_blank">Digital Acorn Limited</a>. </h5>';
			echo '</div>';
		}		

	




<?php

class da_bespoke_flickr {
	protected static $instance = null;

	public static function instance(){
		null === self::$instance AND self::$instance = new self;
		return self::$instance;
	}

	protected function __construct(){
		add_action( 'admin_menu', array($this, 'admin_menu'), 11);
	}
	private function sign($method, $url, $params, $token_secret='') {
		global $basestr;
		$consumer_key = da_get_option('flickr_api_key');
		$consumer_secret = da_get_option('flickr_api_secret');
		$params['oauth_nonce'] = rand();
		$params['oauth_timestamp'] = time();
		$params['oauth_consumer_key'] = $consumer_key;
		$params['oauth_signature_method'] = 'HMAC-SHA1';
		$params['oauth_version'] = '1.0';
		ksort($params);
		if (isset($params['oauth_signature'])) {
			unset($params['oauth_signature']);
		}
		$query = str_replace(array('+', '%7E'), array('%20', '~'), http_build_query($params, '', '&'));
		$basestr = $method.'&'.urlencode($url).'&'.urlencode($query);
		$params['oauth_signature'] = base64_encode(hash_hmac('sha1', $basestr, $consumer_secret.'&'.$token_secret, true));
		return $params;
	}

	private function oauth_header($method, $url, &$params, $token_secret='') {
		global $basestr, $sig;
		$consumer_secret = da_get_option('flickr_api_secret');
		$token = $params['oauth_token'];
		$sigalg = 'HMAC-SHA1';
		ksort($params);
		if (isset($params['oauth_signature'])) {
			unset($params['oauth_signature']);
		}
		$query = str_replace(array('+', '%7E'), array('%20', '~'), http_build_query($params, '', '&'));
		$basestr = $method.'&'.urlencode($url).'&'.urlencode($query);
		$params['oauth_signature'] = urlencode(base64_encode(hash_hmac('sha1', $basestr, $consumer_secret.'&'.$token_secret, true)));
		$sig = $params['oauth_signature'];
		$header = 'OAuth ';
		$sep = '';
		$oauth_fields = array(
			'oauth_nonce', 
			'oauth_timestamp', 
			'oauth_consumer_key', 
			'oauth_token', 
			'oauth_signature',
			'oauth_signature_method', 
			'oauth_version');
		foreach ($oauth_fields as $key) {
			$value = $params[$key];
			$header .= $sep.$key.'="'.$value.'"';
			$sep = ', ';
			unset($params[$key]); // remove oauth fields into headers
		}
		return $header;
	}

	public function admin_menu(){
		add_submenu_page('bespoke', 'Flickr', 'Flickr Settings', 'manage_options', 'flickr', array($this, 'admin_options')); 
	}

	public function admin_options(){
		if (!current_user_can('manage_options'))  
			wp_die( __('You do not have sufficient permissions to access this page.') );

		$fields = array(
			'flickr_api_key'	=> array(
				'name'			=> 'Flickr API key',
				'input_type'	=> 'input'
			),
			'flickr_api_secret'	=> array(
				'name'			=> 'Flickr API secret',
				'input_type'	=> 'input'
			)
		);

		$fields = apply_filters('bespoke_f_flickr_options', $fields);

		echo '<div class="bespokesettings"><h1>Flickr Settings</h1>';

		$need_auth = true;

		if (isset($_REQUEST['oauth_token']) && isset($_REQUEST['oauth_verifier'])) {
			$nonce = $_REQUEST['extra'];
			if ( ! wp_verify_nonce( $nonce, 'bespoke-flickr-wpnonce' ) ) {
				die( 'invalid' ); 
			}
			$url = 'https://www.flickr.com/services/oauth/access_token';
			$params = array(
				'oauth_token' => $_REQUEST['oauth_token'],
				'oauth_verifier' => $_REQUEST['oauth_verifier']
			);
			$params = $this->sign('GET', $url, $params, da_get_option('_oauth_token_secret') );
			$response = wp_remote_get($url.'?'.http_build_query($params));

			$error_message = false;
			if ( is_wp_error( $response ) ) {
				$error_message = $response->get_error_message();
			}
			if (!$error_message) {
				$data = array();
				if (is_array($response) && isset($response['body'])) {
					parse_str($response['body'], $data);
				}
				if (isset($data['oauth_token']) && isset($data['oauth_token_secret'])) {
					da_set_option('_oauth_token', $data['oauth_token']);
					da_set_option('_oauth_token_secret', $data['oauth_token_secret']);				
				} else {
					$error_message = 'Unable to authenticate';
				}
			}
			if ($error_message) {
				echo "Something went wrong communicating with Flickr: $error_message";
			}
		} 

		// look for access token
		if (da_get_option('_oauth_token') && da_get_option('_oauth_token_secret')) {
			$rsp = $this->api('flickr.auth.oauth.checkToken');
			if ( is_wp_error($rsp) ) {
				$error_message = $rsp->get_error_message();
				echo "Something went wrong communicating with Flickr: $error_message";
			} else {
				$perms = esc_html($rsp['oauth']['perms']['_content']);
				$flickr_user = esc_html($rsp['oauth']['user']['fullname']);
				da_set_option('flickr_api_user', $flickr_nsid = $rsp['oauth']['user']['nsid']);
				?>
				<p>This website is successfully authenticated with <strong><?php echo $perms;?></strong> 
					permissions for <strong><?php echo $flickr_user;?> </strong> (NSID=<?php echo $flickr_nsid;?>)</p>
				<p>These permissions can be revoked by logging into your Flickr account and finding the section 
					entitled "Apps You're Using"</p>
				<?php
				$need_auth = false;
			}
		}
				
		if ($need_auth) {
			$url = 'https://www.flickr.com/services/oauth/request_token';
			$params = array(
				'oauth_callback' => admin_url('admin.php?page=flickr'),
			);
			$params = $this->sign('GET', $url, $params );
			if (empty($params['oauth_consumer_key'])) {
				$error_message = 'no API keys specified';
			} else {
				$response = wp_remote_get($url.'?'.http_build_query($params));
				$error_message = false;
				if ( is_wp_error( $response ) ) {
					$error_message = $response->get_error_message();
				}								
			}
			if (!$error_message) {
				$data = array();
				if (is_array($response) && isset($response['body'])) {
					parse_str($response['body'], $data);
					if (!isset($data['oauth_token'])) {
						$error_message = 'no authentication token was returned';
					} else{
						$oauth_token = $data['oauth_token'];
						$oauth_token_secret = $data['oauth_token_secret'];
						da_set_option('_oauth_token_secret', $oauth_token_secret);
						$params = array(
							'oauth_token'=>$oauth_token, 
							'perms'=>'write', 
							'extra'=>wp_create_nonce( 'bespoke-flickr-wpnonce' )
						);
						echo '<br/><a class="button button-secondary" href="https://www.flickr.com/services/oauth/authorize/?'.http_build_query($params).'">Authorise this website to connect to Flickr</a><br/><br/>';

					}
				}
			}
			if ($error_message) {
			   echo "Something went wrong communicating with Flickr: $error_message";
			}

		}

		da_render_global_option_inputs($fields);

		echo '</div>';
	}	

	public function upload($fullpath, $params=array()) {

		$oauth_token =  da_get_option('_oauth_token');
		$oauth_token_secret = da_get_option('_oauth_token_secret');

		// early out if no api key
		if (  !$oauth_token || !$oauth_token_secret )
			return new WP_Error( 'flickr-authentication', __( "Authorisation is not configured") );

		$params = array_merge($params, array(
			'oauth_token' => $oauth_token
		));
		if (isset($params['photo_id']) && $params['photo_id']) {
			$url = 'https://api.flickr.com/services/replace/';
		} else {
			$url = 'https://api.flickr.com/services/upload/';			
		}

		$params = $this->sign('POST', $url, $params, $oauth_token_secret);
		$boundary = md5( $params['oauth_nonce'] . $params['oauth_timestamp'] );
		$header_auth = $this->oauth_header('POST', $url, $params, $oauth_token_secret);

		$headers  = array(
			'content-type' => 'multipart/form-data; boundary=' . $boundary,
			'Authorization' => $header_auth
		);
		$payload = '';
		foreach ( $params as $name => $value ) {
			$payload .= '--' . $boundary . "\r\n";
			$payload .= 'Content-Disposition: form-data; name="' . $name .'"' . "\r\n\r\n";
			$payload .= $value;
			$payload .= "\r\n";
		}
		$payload .= '--' . $boundary . "\r\n";
		$payload .= 'Content-Disposition: form-data; name="photo"; filename="' . basename( $fullpath ) . '"' . "\r\n\r\n";
		$payload .= file_get_contents( $fullpath );
		$payload .= "\r\n";
		$payload .= '--' . $boundary . '--';

		$response = wp_remote_post( $url, array(
			'timeout' => 30,
			'headers' => $headers,
			'body' => $payload
		) );
		if ( is_wp_error( $response ) ) {
			return $response;
		} else {
			try {
				if ($response['response']['code'] != 200) {
					return new WP_Error( $response['response']['code'], 'Invalid code '.$response['body'] );
				}
				$rsp = explode("\n", $response['body']);
				foreach ($rsp as $line) {
					if (preg_match('|<err code="([0-9]+)" msg="(.*)"|', $line, $match)) {
						return new WP_Error( $match[1], "The Flickr API returned the following error: #{$match[1]} - {$match[2]}" );
					} elseif (preg_match("|<photoid[^>]*>(.*)</photoid>|", $line, $match)) {
						return $match[1];
					}
				}
				return new WP_Error( 'flickr-unexpected-response', 'unable to parse the response from Flickr. '.$response['body'] );

			} catch (Exception $e) {
				return new WP_Error( 'flickr-unexpected', 'Unexpected error while processing the response from the upload', $e );
			}
		}			
		return new WP_Error( 'flickr-unexpected', 'Unexpected');
	}

	public function api( $method = '', $params = array(), $cache = 3600 ) {

		$oauth_token =  da_get_option('_oauth_token');
		$oauth_token_secret = da_get_option('_oauth_token_secret');

		// early out if no api key
		if (  !$oauth_token || !$oauth_token_secret || empty( $method ) )
			return false;

		// this is how we cache json responses in transients
		$params_hash = 'bespoke_flickr_' . md5( serialize( array( $method, $params ) ) );

		// turn cache off in debug mode
		if ( WP_DEBUG )
			$cache = false;

		// check cache and fetch if empty
		if ( ! $cache || false === ( $response = get_transient( $params_hash ) ) ) {

			$url = "https://api.flickr.com/services/rest/";
			$params['oauth_token'] = $oauth_token;
			$params['format'] = 'json';
			$params['nojsoncallback'] = 1;
			$params['method'] = $method;
			$params = $this->sign('POST', $url, $params, $oauth_token_secret);

			$response = wp_remote_post( $url, array(
				'body' => $params
			) );

			if ( is_wp_error( $response ) )
				return $response;

			$response = $response[ 'body' ];
			//print_r($response);

			// if not an error code set the transient
			$rsp = json_decode( $response, true );
			if ( $cache && ($rsp['stat'] == 'ok') ) {
				set_transient( $params_hash, $response, $cache ); 
			} elseif ( isset( $rsp['code'] ) ) {
				if (isset($rsp['message'])) {
					$msg = $rsp['message'];
				}
				else {
					$msg = "Flickr error with code ".$rsp['code'];					
				}
				$error = new WP_Error( $rsp['code'], $msg, $rsp );
				return $error;
			}
		}

		return json_decode($response, true);
	}

}

da_bespoke_flickr::instance();

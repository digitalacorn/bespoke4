<?php
class da_bespoke_childpagesubnav {
	protected static $instance = null;


	public static function instance(){
		null === self::$instance AND self::$instance = new self;
		return self::$instance;
	}

	protected function __construct() {
		add_filter('bespoke_f_meta_fields', array($this, 'meta'));
		add_shortcode('childpagesubnav', array($this, 'shortcode'));
		add_action('bespoke_do_before_output', array($this, 'before_output'));
	}

	public function meta($metafields){
		$meta = array();
		$meta['childpagesubnav'] = array(
			'default' 		=> 'middle',
			'input_type' 	=> 'select',
			'name' 			=> 'Show Page subnavigation',
			'post_types' 	=> array('page'),
			'options'		=> array('before' => 'before', 'middle'=>'middle/none', 'after'=>'after'), 
			'description' 	=> 'Choose where subnavigation appears - if middle is selected it will appear where the shortcode [childpagesubnav] is inserted'
		);	
		return array_merge($metafields, $meta);				
	}

	public function before_output() {
		global $post;
		if (isset($post)) {
			$opt = da_get_post_meta(null, 'childpagesubnav');
			if ('before' == $opt) {
				add_action('bespoke_do_before_content', array($this, 'render'));
			}
			else if ('after' == $opt) {
				add_action('bespoke_do_after_content', array($this, 'render'));				
			}
		}

	}

	public function render() {
		global $post;
		$subq = new WP_Query(array(
			'orderby' => 'menu_order', 
			'order' => 'ASC', 
			'posts_per_page' => -1, 
			'post_parent' => $post->ID, 
			'post_type' => 'page')); 

		if ($subq->have_posts()) {
			do_action('bespoke_do_before_childpagessubnav');
			while ($subq->have_posts()) {
				$subq->the_post(); 
				bespoke_content_template( 'childpagessubnav' );
			}			
			do_action('bespoke_do_after_childpagessubnav');
		}
		wp_reset_postdata();
	}

	public function shortcode($atts){
		$atts = shortcode_atts(array(
			), 
			$atts
		);
		global $post;
		$content = '';
		$opt = da_get_post_meta(null, 'childpagesubnav');
		if(('middle'==$opt) && isset($post)){
			ob_start();
			$this->render();
			$content .= ob_get_clean();
		}
		return $content;
	}
}

da_bespoke_childpagesubnav::instance();

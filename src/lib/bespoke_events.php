<?php


	function da_events_timedesc_render_column($holder, $val) {
		$start= da_get_post_meta($holder->post->ID, 'start');
		$end = da_get_post_meta($holder->post->ID, 'end');
		$recurrs = da_get_post_meta($holder->post->ID, 'recurrs');
		$recurrs = $recurrs ? $recurrs : '0';
		$recurrs = DA_Events::$recurring_options[$recurrs];
		if ($start)
			$ret = date('d/M/Y', $start);
		else
			$ret = '';
		if ($end)
			$ret.=' - '.date('d/M/Y', $end);
		if ($start)
			$ret .= ' ('.str_replace('[weekday]', date('l', $start), $recurrs).')';
		if ($val)
			$ret.='<br/><em>'.$val.'</em>';
		return $ret; // $start.$end.$recurrs.$val;
	}		
	
	class DA_Events{
	
		protected static $instance = null;
		private static $capability = false;
		private static $slug;
		
		public static function instance(){
			null === self::$instance AND self::$instance = new self;
			return self::$instance;
		}

		public static $recurring_options = array(
			'0'		=> 'One Off',
			//'30'	=> 'On this day each month',	// TODO, only if needed
			'x7'	=> 'Weekly',
			'x14'	=> 'Fortnightly',
			'W1'	=> '1st [weekday] of month',
			'W2'	=> '2nd [weekday] of month',
			'W3'	=> '3rd [weekday] of month',
			'W4'	=> '4th [weekday] of month',
			'W5'	=> 'last [weekday] of month'
		);
	 
		private function __construct(){
			self::$capability = 'manage_categories';
			self::$slug = apply_filters('bespoke_f_events_slug', 'events');
			add_filter('bespoke_f_meta_fields', array(get_called_class(), 'meta'), 12, 1);
			add_action('init', array(get_called_class(), 'register_posttypes'));
			add_action('pre_get_posts', array(get_called_class(), 'event_order'), 12, 1);
			add_shortcode('event_calendar', array(get_called_class(), 'shortcode'));
			add_action('bespoke_do_dynamic_css', array(get_called_class(), 'css'));
			add_action('bespoke_do_js_docready', array(get_called_class(), 'js'));
			add_action('wp_ajax_ajax_cal_output',  array(get_called_class(), 'ajax_cal_output'));
			add_action('wp_ajax_nopriv_ajax_cal_output',  array(get_called_class(), 'ajax_cal_output'));
		}
		
		public static function css(){
			ob_start();
?>				.da_event_cal_outer_wrapper {text-align: center;}
				table.da_event_cal_outer {width: 100%; margin: 0 0 20px; table-layout: fixed; border-collapse: collapse;}
				table.da_event_cal_outer td {overflow: hidden;}
				tr.header_row td {border: 1px solid #dadada; padding: 2px 0; background: #d9d9db;}
				tr.week_row td {border: 1px solid #dadada;}
				tr.week_row td{height: 49px; vertical-align: top;}
				tr.title_row td {padding: 10px 0; text-align: center;}
				tr.week_row td.da_today {border: 2px solid #333;}
				.da_event_cal_events_wrapper {vertical-align: middle;}
				a.da_event_cal_event_link {background: #ddd; display: block; height: 31px; width: 100%; padding: 7px 0 0; }
				a.da_event_cal_event_link:hover{color: #fff;}
				td.da_event_day .da_event_cal_day_title {background: #505052; color: #DADADA;}
				.month_change:hover {cursor: pointer;}
				td.da_event_cal_title {text-align: center;}
<?php		$css = ob_get_clean();
			echo str_replace(array("\r", "\n", "\t"), '', $css);
		}
		
		public static function js(){
			$options = array('url' => admin_url('/admin-ajax.php?action=ajax_cal_output'));
			echo 'da_bespoke.events.init(' . json_encode($options) . ');';
		}
		
		public static function meta($metafields){

			$meta = array();
		
			$meta['timedesc'] = array(
				'default' 		=> '',
				'input_type' 	=> 'input',
				'priority' 		=> 1,
				'name' 			=> 'Date and Time',
				'post_types' 	=> array('event'),
				'columns'		=> 'da_events_timedesc_render_column',
				'description' 	=> 'Human-readable text describing when this event occurs'
			);		
			
			$meta['start'] = array(
				'default' 		=> 'today',
				'input_type' 	=> 'date',
				'priority' 		=> 1,
				'name'	 		=> 'Starts',
				'post_types' 	=> array('event'),
				'description' 	=> 'Select start date for this event (inclusive)'
			);
			
			$meta['end'] = array(
				'default' 		=> '',
				'input_type' 	=> 'date',
				'priority' 		=> 1,
				'name' 			=> 'Ends',
				'post_types' 	=> array('event'),
				'description' 	=> 'Please enter an end date (inclusive). For recurring events this will be when recurring stops'
			);

			$meta['recurrs'] = array(
				'default' 		=> '',
				'input_type'	=> 'select',
				'priority' 		=> 2,
				'name' 			=> 'Recurring Event',
				'options' 		=> self::$recurring_options,
				'post_types' 	=> array('event'),
				'description' 	=> 'How often does this event occur?<br/>One-off events start and end on the dates provided inclusively<br/>Recurring events are always 1 day duration, and recurr until the end date (or forever if no end-date is provided).'
			);
			
			$meta['event_venue'] = array(
				'default' 		=> '',
				'input_type' 	=> 'input',
				'priority' 		=> 1,
				'name' 			=> 'Event Venue',
				'post_types' 	=> array('event'),
				'description' 	=> 'Where is this event taking place?'
			);	
			
			return array_merge($metafields, apply_filters('bespoke_f_events_metafields', $meta));
		}
		
		public static function register_posttypes(){
			$pt_args = array(
				'labels' 				=> auto_post_type_labels('Event', 'Events'),
				'public' 				=> true,
				'description' 			=> '',
				'publicly_queryable' 	=> true,
				'show_ui' 				=> true, 
				'show_in_menu' 			=> true, 
				'query_var'				=> true,
				'show_in_nav_menus'		=> false,
				'rewrite' 				=> array('slug' => self::$slug),
				'capability_type' 		=> 'post',
				'has_archive' 			=> true, 
				'hierarchical' 			=> false,
				'menu_icon' 			=> get_template_directory_uri() . '/img/favicon.ico',
				'supports' 				=> array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'wpautop')
			); 
			register_post_type('event', $pt_args);	

			$cat_args = array(
				'hierarchical' 				=> true,
				'labels' 					=> auto_tax_labels('Event Category', 'Event Categories'),
				'show_ui' 					=> true,
				'show_tagcloud' 			=> false,
				'capabilities' 				=> array(
					'manage_terms' 	=> self::$capability,
					'edit_terms' 	=> self::$capability,
					'delete_terms' 	=> self::$capability,
					'assign_terms' 	=> self::$capability,
				),
				'rewrite' 					=> array('slug' => apply_filters('bespoke_f_events_category_slug', 'event-category')),
				'update_count_callback' 	=> '_update_post_term_count',
				'query_var' 				=> true				
			);
			register_taxonomy('event_cat', array('event'), $cat_args);			
		}
		
		public static function ajax_cal_output(){
			$month = null; $year = null; $id=null;
			if(isset($_REQUEST['month']))
				$month = $_REQUEST['month'];
			if(isset($_REQUEST['year']))
				$year = $_REQUEST['year'];
			if(isset($_REQUEST['calendar_id']))
				$calendar_id = sanitize_key($_REQUEST['calendar_id']);
			do_action('da_events_berore_ajax_draw_calendar', $calendar_id);
			echo self::draw_calendar($month, $year, $calendar_id);
			die();
		}

		public static function shortcode($atts) {
			$atts = shortcode_atts(array(
					'month' 	=>  date('m'),
					'year' 		=>  date('Y'),
				), 
				$atts
			);
			return self::draw_calendar($atts['month'], $atts['year']);
		} 
		
		private static function event_args($start = false, $end = false){

			if(!$start || !$end)
				return array();
			
			$args = array(
				'post_type' 		=> 'event',
				'meta_query'		=> array(
					'relation'=>'AND',
					array(
						'key' 		=> da_meta_field('start'),
						'value' 	=> $end,
						'compare' 	=> '<='
					),
					array(
						'relation'	=> 'OR',
						array(
							// event has started and hasn't ended
							'key' 		=> da_meta_field('end'),
							'value' 	=> $start,
							'compare' 	=> '>='
						),
						array(
							// event is recurring and had no end date specified (implies recurrs forever)
							'relation' 	=> 'AND',
							array(
								'key' 		=> da_meta_field('end'),
								'value' 	=> '',
								'compare' 	=> 'NOT EXISTS'
							),
							array(
								'key' 		=> da_meta_field('recurrs'),
								'value' 	=> '0',
								'compare' 	=> '!='
							)
						),
						array(
							// event is not recurring and had no end date specified (implies one day event)
							'relation' 	=> 'AND',
							array(
								'key' 		=> da_meta_field('end'),
								'value' 	=> '',
								'compare' 	=> 'NOT EXISTS'
							),
							array(
								'key' 		=> da_meta_field('start'),
								'value' 	=> $start,
								'compare' 	=> '>='
							),
							array(
								'key' 		=> da_meta_field('recurrs'),
								'value' 	=> '0',
								'compare' 	=> '='
							)
						)
					)
				),
				'posts_per_page' 	=> -1
			);

			return $args;			
		}

		private static function fetchmonthevents($month = null, $year = null){
			
			if (is_null($month))
				$month = date('m');
			if(is_null($year))
				$year = date('Y');
			if($month < 1 || $month > 12)
				$month = date('m');
			
			if (!is_numeric($month) || !is_numeric($year))
				return;			
			
			global $post;
			$start = mktime(0, 0, 0, $month, 1, $year);
			$end = mktime(23, 59, 59, $month + 1, 0, $year);
			$query = new WP_Query(self::event_args($start, $end));
			if (!empty($query)){
				$events = array();
				while ($query->have_posts()){ 
					$query->the_post();
					$id = get_the_id();
					if (!in_array($id, $events)){
						$events[$id] = apply_filters('bespoke_f_events_extract_meta', array(
							'id' 		=> $id,
							'title' 	=> get_the_title(),
							'link' 		=> get_permalink(),
							'start' 	=> da_get_post_meta($id, 'start'),
							'end' 		=> da_get_post_meta($id, 'end'),
							'recurrs' 	=> da_get_post_meta($id, 'recurrs'),
							'timedesc' 	=> da_get_post_meta($id, 'timedesc')
						), $id);
					}
				}				
				wp_reset_postdata();
				
				if (!empty($events)){

					$days = array();
					$numDays = date('d', $end);

					$totwkday = array(0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0);
					for ($day = 1; $day <= $numDays ; $day++){
						$dt = mktime(0, 0, 0, $month, $day, $year);
						$dow = date('w', $dt);
						$totwkday[$dow] += 1;
					}

					$nthwkday = array(0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0);
					for ($day = 1; $day <= $numDays ; $day++){
						$dt = mktime(0, 0, 0, $month, $day, $year);
						$dow = date('w', $dt);
						$nthwkday[$dow] += 1;
						$status = array();
						foreach($events as $id => $event){
							$ev_dow = date('w', $dt);
							$daydelta = floor(($dt - $event['start']) / 86400);
							if (($event['start'] <= $dt) && (($event['end'] >= $dt) || ('' == $event['end']))){
								switch ($event['recurrs']){
									case '0':
										$match = $event['end']=='' ? ($event['start'] == $dt) : true;//$event['start']==$dt;
										break;
									case 'x7':
										$match = 0==($daydelta%7);
										break;
									case 'x14':
										$match = 0==($daydelta%14);
										break;
									case 'W1':
										$match = (0==($daydelta%7)) && (1==$nthwkday[$dow]);
										break;
									case 'W2':
										$match = (0==($daydelta%7)) && (2==$nthwkday[$dow]);
										break;
									case 'W3':
										$match = (0==($daydelta%7)) && (3==$nthwkday[$dow]);
										break;
									case 'W4':
										$match = (0==($daydelta%7)) && (4==$nthwkday[$dow]);
										break;
									case 'W5':
										$match = (0==($daydelta%7)) && ($nthwkday[$dow]==$totwkday[$dow]);
										break;
									default:
										$match = false;
										break;
								}
								if ($match) {
									$status['events'][] = $id;
									$status = apply_filters('bespoke_f_events_status', $status, $event);
								}
									
							}
						}
						$days[$day] = $status;
					}

					return array(
						'events' => $events,
						'days' => $days
					);
				}
			}
			
			return;
		}

		public static function draw_calendar($month = null, $year = null, $calendar_id = null){
			
			if (is_null($month))
				$month = date('m');
			if(is_null($year))
				$year = date('Y');
			if($month < 1 || $month > 12)
				$month = date('m');
			
			if (!is_numeric($month) || !is_numeric($year))
				return;

			$calendar_id = $calendar_id ? $calendar_id : 'default';
		
			$today = time();
			$draw_day = mktime(0, 0, 0, $month, 1, $year);
			$lastmonth = mktime(0, 0, 0, $month - 1, 1, $year);
			$nextmonth = mktime(0, 0, 0, $month + 1, 1, $year);
			$weekdays = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
			$first_row = true;
			$padd_count_start = date('N', $draw_day) - 1;
			$month_events = array();
			$event_data = self::fetchmonthevents($month, $year);
			$events = !empty($event_data['events']) ? $event_data['events'] : array();
			$days = $event_data['days'];
			
			$out = '<div class="da_event_cal_outer_wrapper">';
				
				$out .= '<table class="da_event_cal_outer">';
			
					$out .= '<tr class="da_event_cal title_row">';
						$out .= '<td class="da_event_cal_month da_event_cal_month_down">';
							$out .= '<span data-calendar_id="'.esc_attr($calendar_id).'" data-month="' . date('n', $lastmonth) . '" data-year="' . date('Y', $lastmonth) . '" class="month_change da_event_cal_month_down_link">&laquo;</span>';
						$out .= '</td>';
						$out .= '<td colspan="5" class="da_event_cal_title">' . date('F', $draw_day) . ' ' . date('Y', $draw_day) . '</td>';
						$out .= '<td class="da_event_cal_month da_event_cal_month_up">';
							$out.= '<span data-calendar_id="'.esc_attr($calendar_id).'" data-month="' . date('n', $nextmonth) . '" data-year="' . date('Y', $nextmonth) . '" class="month_change da_event_cal_month_up_link">&raquo;</span>';
						$out .= '</td>';
					$out .= '</tr>';
					
					$out .= '<tr class="da_event_cal header_row">';
					foreach($weekdays as $weekday)
						$out .= '<td class="da_eventcal_header day_' . strtolower($weekday) . '">' . $weekday . '</td>';
					$out .= '</tr>';

					$out .= '<tr class="da_event_cal week_row">';
					
					for($i = 0; $i < $padd_count_start; $i++)
						$out .= '<td class="da_event_cal_pad"></td>';
					
					foreach ($events as $event){
						$start = $event['start'];
						if (!isset($month_events[$start]))
							$month_events[$start] = array();
						$month_events[$start][] = $event;
					}

					while(date('m', $draw_day) == $month){

						if (date('N', $draw_day) == 1 && !$first_row)
							$out .= '<tr class="da_event_cal week_row">';
						
						$has_event = false;
						$da_event_day_classes = array();
						if (date('d', $draw_day) == date('d', $today) && date('n', $draw_day) == date('n', $today) && date('Y', $draw_day) == date('Y', $today))
							$da_event_day_classes[] = 'da_today';
						$day = date('j', $draw_day);
						if (is_array($days) && array_key_exists($day, $days) && isset($days[$day]['events'])){
							$total_events = count($days[$day]['events']);
							$has_event = apply_filters('bespoke_f_events_day_has_event', $total_events>0, $days[$day]);
							if ($has_event) {
								$da_event_day_classes[] = 'da_event_day';
							}
						}
						$da_event_day_classes = apply_filters('bespoke_f_classes_event_day', $da_event_day_classes, $days[$day]);
                    
						$out .= '<td class="da_event_cal_day ' . date('D', $draw_day).' ' . join(' ', $da_event_day_classes) . '">';

						$day_content = apply_filters('bespoke_f_events_day_render_override', '', $days[$day], $day, $events);
						if (!empty($day_content)) {
							$out .= $day_content;
						} else {
							$out .= '<div class="da_event_cal_day_title">' . $day . '</div>';
							$sep = '';
							if($has_event){
								$out .= '<div class="da_event_cal_events_wrapper">';
									if ($total_events > 1){
										$url = home_url() . '/'.self::$slug.'/';
										$url = add_query_arg('ids', join($days[$day]['events'],','), $url);
										$title = 'See events for this day';
									}
									else {
										$url = $events[$days[$day]['events'][0]]['link'];
										$title = 'View event - ' . $events[$days[$day]['events'][0]]['title'];
									}
									$out .= '<a class="da_event_cal_event_link" title="' . $title . '" href="' . $url . '">'. $total_events .'</a>';
								$out .= '</div>';
							}							
						}
                    
						$out .= '</td>';
						if(date('N', $draw_day) == 7){
							$out .= '</tr>';
							$first_row = false;
						}
						$draw_day = mktime(0, 0, 0, date('m', $draw_day), date('j', $draw_day)+1, date('Y', $draw_day));
					}
					
					$padd_count_end = 7 - date('N', $draw_day);
					for($i = 0; $i <= $padd_count_end; $i++)
						$out .= '<td class="da_event_cal_pad"></td>';
					$out .= '</tr>';

				$out .= '</table>';
				
			$out .= '</div>';

			return $out;
		}

		public static function specific_date_events($query) {
			$dd = explode(',',$_GET['ids']);
			$query->set('post__in', $dd);
			return $query;
		}

		public static function next($fromnow=null) {
			// the next non recurring event
			$now = $fromnow ? $fromnow : strtotime('today midnight');

			$posts = get_posts(array(
				'post_type' 		=> 'event',
				'orderby' 			=> 'meta_value_num', 
				'meta_key' 			=> da_meta_field('start'),
				'order'				=> 'ASC',
				'meta_query'		=> array(
					'relation'=>'AND',
					array(
						// non recurring events
						'key' 		=> da_meta_field('recurrs'),
						'value' 	=> '0',
						'compare' 	=> '='
					),
					array(
						'relation'	=> 'OR',
						array(
							// event has started and hasn't ended
							'key' 		=> da_meta_field('end'),
							'value' 	=> $now,
							'compare' 	=> '>='
						),
						array(
							// event is not recurring and had no end date specified (implies one day event)
							'relation' 	=> 'AND',
							array(
								'key' 		=> da_meta_field('end'),
								'value' 	=> '',
								'compare' 	=> 'NOT EXISTS'
							),
							array(
								'key' 		=> da_meta_field('start'),
								'value' 	=> $now,
								'compare' 	=> '>='
							)
						)
					)
				),
				'posts_per_page' 	=> 1
			));
			return current($posts);
		}
		public static function prev($fromnow=null) {
			// the last non recurring event
			$now = $fromnow ? $fromnow : strtotime('today midnight');

			$posts = get_posts(array(
				'post_type' 		=> 'event',
				'orderby' 			=> 'meta_value_num', 
				'meta_key' 			=> da_meta_field('start'),
				'order'				=> 'DESC',
				'meta_query'		=> array(
					'relation'=>'AND',
					array(
						// non recurring events
						'key' 		=> da_meta_field('recurrs'),
						'value' 	=> '0',
						'compare' 	=> '='
					),
					array(
						'relation'	=> 'OR',
						array(
							// event has ended
							'key' 		=> da_meta_field('end'),
							'value' 	=> $now,
							'compare' 	=> '<'
						),
						array(
							// event is not recurring and had no end date specified (implies one day event)
							'relation' 	=> 'AND',
							array(
								'key' 		=> da_meta_field('end'),
								'value' 	=> '',
								'compare' 	=> 'NOT EXISTS'
							),
							array(
								'key' 		=> da_meta_field('start'),
								'value' 	=> $now,
								'compare' 	=> '<'
							)
						)
					)
				),
				'posts_per_page' 	=> 1
			));
			return current($posts);
		}
			
		public static function event_order($query){
			if (!$query->is_main_query() || is_admin())
				return;
			
			if ($query->is_post_type_archive('event') || is_tax('event_cat')){
				$query->set('post_type', 'event');
				$query->set('meta_key', da_meta_field('start'));
				$query->set('orderby', 'meta_value_num');
				$query->set('order', 'ASC');
				if (isset($_GET['ids'])) {
					return self::specific_date_events($query);
				}
				$endcheck = strtotime('today midnight');
				$end_compare = '>=';
				if (isset($_GET['past'])) {
					$end_compare = '<';
					$query->set('order', 'DESC');
				}
				$query->set('meta_query', apply_filters('bespoke_f_events_archive_meta_query', array(
						'relation' => 'OR',
						array(
							'relation' => 'AND',
							array(
								'key' => da_meta_field('end'),
								'value' => $endcheck,
								'compare' => $end_compare
							),
							array(
								'key' => da_meta_field('recurrs'),
								'value' => '0',
								'compare' => '='
							)
						),
						array(
							'relation' => 'AND',
							array(
								'key' => da_meta_field('end'),
								'value' => '',
								'compare' => 'NOT EXISTS'
							),
							array(
								'key' => da_meta_field('start'),
								'value' => $endcheck,
								'compare' => $end_compare
							),
							array(
								'key' => da_meta_field('recurrs'),
								'value' => '0',
								'compare' => '='
							)
						)
					))
				);
			}
		}
	}
	
	class DA_Events_Upcoming_Widget extends WP_Widget {
		
		function __construct() {
			parent::__construct(false, $name = 'Upcoming Events');
		}
		public function widget($args, $instance) {
			global $post;
			$now = strtotime('today midnight');
			$query = array(
				'post_type' 		=> 'event',
				'orderby' 			=> 'meta_value_num', 
				'meta_key' 			=> da_meta_field('start'),
				'order'				=> 'ASC',
				'meta_query'		=> array(
					'relation'=>'AND',
					array(
						// non recurring events
						'key' 		=> da_meta_field('recurrs'),
						'value' 	=> '0',
						'compare' 	=> '='
					),
					array(
						'relation'	=> 'OR',
						array(
							// event has started and hasn't ended
							'key' 		=> da_meta_field('end'),
							'value' 	=> $now,
							'compare' 	=> '>='
						),
						array(
							// event is not recurring and had no end date specified (implies one day event)
							'relation' 	=> 'AND',
							array(
								'key' 		=> da_meta_field('end'),
								'value' 	=> '',
								'compare' 	=> 'NOT EXISTS'
							),
							array(
								'key' 		=> da_meta_field('start'),
								'value' 	=> $now,
								'compare' 	=> '>='
							)
						)
					)
				),
				'posts_per_page' 	=> 5
			);
			if (isset($post) && $post->ID) {
				$query['post__not_in'] = array($post->ID);
			}

			$posts = get_posts($query);
			if (!is_wp_error($posts) && is_array($posts) && count($posts)) {
				echo $args['before_widget'];
				if ( ! empty( $instance['title'] ) ) {
					echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
				}
				echo '<ul>';
				foreach ($posts as $post) {
					setup_postdata($post);
					echo '<li><a href="'.esc_url(get_the_permalink($post)).'">'.get_the_title($post).'</a></li>';
				}
				echo '</ul>';
				wp_reset_postdata();
				// list
				echo $args['after_widget'];
			}
		}
		public function form( $instance ) {
			$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Upcoming Events', 'text_domain' );
			?>
			<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>
			<?php 
		}

		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			return $instance;
		}
	}

	function register_DA_Events_Upcoming_Widget() {
		register_widget("DA_Events_Upcoming_Widget");
	}	
	add_action('widgets_init', 'register_DA_Events_Upcoming_Widget');

	DA_Events::instance();
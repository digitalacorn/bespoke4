<?php
		global $da_attachments_allowed_types;
		//valid file types
		$da_attachments_allowed_types = array(
			'application/pdf' => 'pdf',
			'application/zip' => 'zip',
			'application/vnd.ms-excel' => 'xls',
			'application/msword' => "doc",
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'doc'
		);


		add_action('init', 'da_attachments_init');
		function da_attachments_init(){
			add_action('bespoke_do_dynamic_css', 'da_attachments_css');
			add_shortcode('alluploads', 'da_bespoke_attachments_shortcode');
		}

		function da_attachments_css() {
			echo apply_filters('bespoke_f_attachment_css', da_attachments_do_CSS());	
		}
		
		function da_attachments_do_CSS(){
			ob_start();
?>
			.attachment-entry.pdf {background-image: url('<?php echo apply_filters('da_attachments_pdf_icon_uri', get_template_directory_uri() . '/img/' . 'pdf.png');?>');}
			.attachment-entry.doc {background-image: url('<?php echo apply_filters('da_attachments_word_icon_uri', get_template_directory_uri() . '/img/' . 'word.png');?>');}
			.attachment-entry.xls {background-image: url('<?php echo apply_filters('da_attachments_xls_icon_uri', get_template_directory_uri() . '/img/' . 'xls.png');?>');}
			.attachment-entry.zip {background-image: url('<?php echo apply_filters('da_attachments_zip_icon_uri', get_template_directory_uri() . '/img/' . 'zip.png');?>');}
			.attachment-entry.unknown {background-image: url('<?php echo apply_filters('da_attachments_unknown_icon_uri', get_template_directory_uri() . '/img/' . 'unknown.png');?>');}
<?php					
			$css = ob_get_clean();
			
			return str_replace(array("\r", "\n", "\t"), '', $css);
		}

		add_action('wp', 'da_attachments_setup');
		function da_attachments_setup() {
			$da_attachments_option = da_get_option('automatic_attachments');
			$da_attachments_option = $da_attachments_option ? $da_attachments_option : 'off';
			if ($da_attachments_option!='off') {
				if ($da_attachments_option=='after_loop')
					add_action('bespoke_do_after_content', 'DA_ListAttachments_after', apply_filters('bespoke_f_attachment_after_priority', 10));
				else
					add_filter('the_content', 'DA_ListAttachments', 10, 1);			
			}
		}
		
		//get some file data
		function da_attachments_getSize($attachID){
			$file = get_attached_file($attachID);
			$bytes = filesize($file);
			if (isset($bytes) && $bytes != 0)
			{
				$s = array('B', 'KB', 'MB', 'GB');
				$e = floor(log($bytes)/log(1024));
				return sprintf('%.1f '.$s[$e], ($bytes/pow(1024, floor($e))));
			}
		}

		function DA_ListAttachments_after(){
			if (is_singular())
				echo DA_ListAttachments('');
		}

		//generate the html for the downloads
		function DA_ListAttachments($content, $all=false){

			if (!apply_filters('bespoke_f_automatic_attachments_enabled', true)) 
				return $content;

			global $post;

			
			if (!$post)
				return $content;

			$args = array(
					'post_type' => 'attachment',
					'post_mime_type' => join(',', array('application','text')),
					'numberposts' => -1,
					'post_status' => 'inherit',
					'post_parent' => $all ? null : $post->ID, 
					'orderby' => 'title',
					'order' => 'ASC'		
				);
				
				$s = "";
			$attachments = get_posts($args);
			$listclass = '';
			if ($all) {
				$listclass .= ' all';
			}
			if (count($attachments) > apply_filters('bespoke_f_attachment_minlistnum', 9)) {
				$listclass .= ' many';
			}
			if ($attachments) 
			{
				$s.='<a name="downloads"></a><div class="attachment-block'.$listclass.'">';
				foreach ($attachments as $attachment){
					$s.=da_render_attachment($attachment);
				}
				$s.='</div>';
			}	
			return $content.$s;
				
		}

		function da_render_attachment_id($attachment_id, $filename=null) {
			$attachment = get_post($attachment_id);
			return $attachment ? da_render_attachment($attachment, $filename) : '';
		}

		function da_render_attachment($attachment, $filename=null) {
			global $da_attachments_allowed_types;
			if (isset($da_attachments_allowed_types[$attachment->post_mime_type]))
				$ext = $da_attachments_allowed_types[$attachment->post_mime_type];
			else
				$ext = 'unknown';
			$filename = $filename ? $filename : ucfirst($attachment->post_title);
			$s= '<div class="attachment-entry-wrap">';
			$s.= '<div data-mime="'.esc_attr($attachment->post_mime_type).'" class="' . 'attachment-entry ' . $ext . '">';
			$s.= '<div class="filename">' . $filename . '</div><div class="size">' . da_attachments_getSize($attachment->ID) . '</div>';
			$s.= '<div class="overlay"><a href="' . wp_get_attachment_url($attachment->ID) . '" rel="noopener" target="_blank" data-gaevent="attachment,download"></a></div></div></div>';
			return $s;
		}

		function da_bespoke_attachments_shortcode($atts){
			$atts = shortcode_atts(array(
				), 
				$atts
			);
			$content = DA_ListAttachments('', true);

			// and disable auto attachments now shortode is called
			add_filter('bespoke_f_automatic_attachments_enabled', '__return_false');

			return $content;
		}

//admin side

		add_action('admin_menu', 'bespoke_attachment_menu');

		function bespoke_attachment_menu(){
			add_submenu_page('bespoke', 'Attachments', 'Attachment Settings', 'manage_options', 'attachment_options', 'attachment_options'); 
		}

		function attachment_options(){
			if (!current_user_can('manage_options'))  
				wp_die( __('You do not have sufficient permissions to access this page.') );
		
			$options = array('off' => 'Disabled', 'on' => 'Enabled: filter into content', 'after_loop' => 'Enabled: after content (recommended)');
			$fields = array(
					'automatic_attachments' => array(
						'name'				=> 'Automatic attachment icons',
						'description'		=> 'When you upload files to a post or page we can automatically offer these as a download option on that page with an icon.',
						'input_type'		=> 'select',
						'default_value' 	=> 'off',
						'options'			=> $options
					)
			);
			echo '<div class="bespokesettings"><h1>Automatic Attachment Icons</h1>';
				da_render_global_option_inputs($fields);
			echo '</div>';
		}
		
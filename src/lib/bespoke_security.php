<?php

	if (!defined('ABSPATH')) exit;

		add_action('admin_menu', 'bespoke_security_menu', 11);
		function bespoke_security_menu(){
			add_submenu_page('bespoke', 'Security', 'Security Settings', 'manage_options', 'da_security_options', 'da_security_options'); 
		}

		function da_security_options(){
			if (!current_user_can('manage_options'))  
				wp_die( __('You do not have sufficient permissions to access this page.') );

			$fields = array(
				'comment_form_q'	=> array(
					'name'			=> 'Captcha question for comment forms',
					'description'	=> 'This should be an easy to answer question for any human. Ideally it should be specifically related to your website.<br/>Syntax: question?answer|question2?multiple:valid:answers|question3?answer3',
					'input_type'	=> 'textarea'
				),
				'login_q' 			=> array(
					'name'			=> 'Extra login question',
					'description'	=> 'This helps prevent brute-force attacks against your site. It is not an excuse to use poor quality passwords. <br/>Unless you are open to community membership; a question which only members of your company can answer is ideal.',
					'input_type'	=> 'text'
				),
				'login_a' 			=> array(
					'name'			=> 'Extra login answer',
					'description'	=> 'All users must enter this value to log in to the admin area of the site.',
					'input_type'	=> 'text'
				)
			);
			
			echo '<div class="bespokesettings"><h1>Security Settings</h1>';
				da_render_global_option_inputs($fields);
			echo '</div>';
		}
		
// COMMENT QUIZ

		add_action('init', 'bespoke_comment_quiz_init');
		function bespoke_comment_quiz_init(){
			add_action('comment_form_after_fields', 'bespoke_comment_quiz_fields', 99);
			add_filter('preprocess_comment', 'bespoke_comment_quiz_check');			
		}

		function bespoke_get_quiz(){
			$da_quiz = da_get_option('comment_form_q');

			if (0==strlen($da_quiz))
				return FALSE;

			$quiz = explode("|",$da_quiz);
			foreach ($quiz as &$q) {
				$q = explode("?", $q, 2);
				$q[0] = $q[0]."?";
				$q[1] = explode(":", $q[1]);
			}
			if (0==count($quiz))
				return FALSE;
			return $quiz;
		}

		function bespoke_comment_quiz_fields(){
			$quiz = bespoke_get_quiz();
			if (false === $quiz)
				return;

			$index = rand(0, count($quiz)-1);
			echo '<p>
				<label for="daquizanswer">'.$quiz[$index][0].'</label> <span class="required">*</span>
				<input id="daquizanswer" type="text" autocomplete="off" name="daquizanswer" value="" size="30" maxlength="50"/>
				<input id="daquizquestion" type="hidden" name="daquizquestion" value="'.$index.'"/>
				</p>';
				
			add_action('wp_footer', 'bespoke_comment_footer', 99);
		}

		function bespoke_comment_quiz_check($comment){
			if ( is_user_logged_in() )
				return $comment;
		
			if ( $comment['comment_type'] != '' && $comment['comment_type'] != 'comment' )
				 return $comment;	// not for trackbacks

			$quiz = bespoke_get_quiz();
			if (FALSE === $quiz)
				return $comment;

			if ( isset($_REQUEST['daquizanswer']) && "" == $_REQUEST['daquizanswer'])
				wp_die( __('Please fill the form.', 'da_bespoke' ) );

			if ( isset($_REQUEST['daquizquestion']) && isset($_REQUEST['daquizanswer'])){
				$index = intval(trim($_REQUEST['daquizquestion']));
				if ($index>=0 && $index<count($quiz)){
					$answer = trim($_REQUEST['daquizanswer']);
					if (in_array($answer, $quiz[$index][1]))
						return($comment);				
				}
			} 
			wp_die( __('Oops, please try again', 'da_bespoke'));
		}

		function bespoke_comment_footer(){
?>
			<script type="text/javascript">
				//<!--
				jQuery(function(){
					var tabindex = jQuery('#commentform').find('input,select,textarea').first().attr("tabindex");
					jQuery('#commentform').find('input,select,textarea').each(function() {
						if (this.type != "hidden") {
							jQuery(this).attr("tabindex", tabindex++);
						}
					});
				});
				//-->
			</script>		
<?php
		}

// EXTRA FIELD ON LOGIN

		add_action('login_form','bespoke_security_login_field');
		function bespoke_security_login_field(){
			$q = da_get_option('login_q');
			if (!empty($q)){
?>				<p>
					<label for="da_login_check"><?php echo $q; ?><br>
					<input type="password" size="20" value="" class="input" id="da_login_check" name="da_bespoke_security_check"></label>
				</p>
<?php		}
		}

		add_filter('authenticate', 'bespoke_security_authenticate', 10, 3);
		function bespoke_security_authenticate($user, $username, $password){
			$q = da_get_option('login_q');

			if (!empty($q) && !empty($username)){
				$response = $_POST['da_bespoke_security_check'];
				$user = get_user_by('login', $username );
				$answer = da_get_option('login_a');
				// for per-user values: $answer=get_user_meta($user->ID, 'my_ident_code', true);
				
				if(!$user || $response !=$answer){
					//User note found, or no value entered or doesn't match stored value - don't proceed.
					remove_action('authenticate', 'wp_authenticate_username_password', 20); 
					//Create an error to return to user
					return new WP_Error( 'human_check', __("<strong>ERROR</strong>: Failed additional security checks.") );
				}		
			}

			//Make sure you return null 
			return null;
		}

		// Extra field on create account
		add_filter( 'registration_errors','bespoke_security_create_account_field_check' );
		add_action( 'register_form',   'bespoke_security_create_account_field' , 20);
		function bespoke_security_create_account_field(){
			$q = da_get_option('login_q');
			if (!empty($q)){
?>				<p>
					<label for="da_login_check"><?php echo $q; ?></label>
					<input type="text" size="20" value="" class="input" id="da_login_check" name="da_bespoke_security_check"/>
				</p>
<?php		}	
		}
		
		function bespoke_security_create_account_field_check($errors){
			$q = da_get_option('login_q');
			if (!empty($q)){
				$response = $_POST['da_bespoke_security_check'];
				$answer = da_get_option('login_a');
				if($response !=$answer)
					$errors->add('human_check', __('<strong>ERROR</strong>: Failed additional security checks.'));
			}
			return $errors;
		}		

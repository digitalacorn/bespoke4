<?php

	if (!defined('ABSPATH')) exit;

	define('BESPOKE_DATE_FORMAT', 'd-m-Y');  /* must match the same format in JS */
	define("DA_SALT1", NONCE_SALT.'kjnbkachvfljsdcrdfklgj$sdnvk;sjfx?<');
	define("DA_SALT2", NONCE_SALT.'Vkasdnlmdcjnshbc%^4efkbnJSVB48£dba');

	function DAhash($value, $zone){
		return sha1($zone.DA_SALT1.md5($value.DA_SALT2));
	}

	if ( ! function_exists( 'da_meta_field' ) ) :
		function da_meta_field($field) {
			if ('_' === substr($field, 0, 1)) {
				return '_da_bespoke' . $field . '_value';			
			}
			else {
				return 'da_bespoke_' . $field . '_value';			
			}
		}
	endif;

	function da_get_option($field){
		return get_option(da_meta_field($field));
	}

	function da_get_post_meta($post_id = null, $field = null, $single = true){
		if (is_null($field))
			return;
		if (is_null($post_id)){
			global $post;
			if (isset($post))
				$post_id = $post->ID;
			else
				return;
		}
		$meta = get_post_meta($post_id, da_meta_field($field), $single);
		return $meta;	
	}

	function auto_post_type_labels($singular, $plural, $overrule=array()){
		// do not use if localising
		return  array_merge(array(
			'name' => _x($plural, 'post type general name'),
			'singular_name' => _x($singular, 'post type singular name'),
			'add_new' => _x('Add New', $singular),
			'add_new_item' => __('Add New '.$singular),
			'edit_item' => __('Edit '.$singular),
			'new_item' => __('New '.$singular),
			'all_items' => __('All '.$plural),
			'view_item' => __('View '.$singular),
			'search_items' => __('Search '.$plural),
			'not_found' =>  __('No '.$plural.' found'),
			'not_found_in_trash' => __('No '.$plural.' found in Trash'), 
			'parent_item_colon' => '',
			'menu_name' => $plural
			), $overrule);
	}

	function auto_tax_labels($singular, $plural, $overrule=array()){
		return array_merge(array(
			'name' => _x( $singular, 'taxonomy general name' ),
			'singular_name' => _x( $singular, 'taxonomy singular name' ),
			'search_items' =>  __( 'Search '.$plural ),
			'popular_items' => __( 'Popular '.$plural ),
			'all_items' => __( 'All '.$plural ),
			'parent_item' => __( 'Parent '.$singular ),
			'parent_item_colon' =>  __( $singular.':' ),
			'edit_item' => __( 'Edit '.$singular ), 
			'update_item' => __( 'Update '.$singular ),
			'add_new_item' => __( 'Add New '.$singular ),
			'new_item_name' => __( 'New '.$singular ),
			'separate_items_with_commas' => __( 'Separate '.$plural.' with commas' ),
			'add_or_remove_items' => __( 'Add or remove '.$plural ),
			'choose_from_most_used' => __( 'Choose from the most used '.$plural ),
			'menu_name' => __( $plural )
		), $overrule);
	}

	function bespoke_kses($content, $context=null) {
		// helper to escape wysiwyg, default single argument behaviour is to escape in the 'post' context.
		$context = is_null($context) ? 'post' : $context;
		$allowed_html = is_array($context) ? $context : wp_kses_allowed_html($context);
		return wp_kses($content, $allowed_html);
	}

	function getDomain() {
		$url_parts = parse_url( site_url() ); 
		if ( ! $url_parts ) {
			return '';	// abort with empty string
		}
		$scheme = $url_parts['scheme'];
		$port   = array_key_exists('port', $url_parts) ? $url_parts['port'] : 80;
		$host   = $url_parts['host'];
		$port   = 80 == $port ? '' : $port;
		$port   = 'https' == $scheme && 443 == $port ? '' : $port;
		$port   = ! empty( $port ) ? ":$port" : '';
		return $scheme . '://' . $host . $port;
	}



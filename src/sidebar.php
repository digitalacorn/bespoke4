<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bespoke
 */

if (!defined('ABSPATH')) exit;

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="<?php bespoke_classes('aside', array('widget-area')); ?>" role="complementary">
	<?php do_action('bespoke_do_before_sidebar');?>
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
	<?php do_action('bespoke_do_after_sidebar');?>
</aside><!-- #secondary -->

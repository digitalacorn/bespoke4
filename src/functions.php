<?php
/**
 * bespoke functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bespoke
 */

if (!defined('ABSPATH')) exit;

class Bespoke {
	protected static $all_features = array(
										'helpers', 
										'actions', 
										'markup', 
										'admin', 
										'postmeta', 
										'dynamicfiles',
										'daadminwidget', 
										'cookiewarning',
										'security',
										'attachments',
										'carousel',
										'optimise',
										'events',
										'fontawesome',
										'flickr',
										'childpagesubnav',
										'social',
										'legacy'
										);

	protected static $required_features = array('helpers', 'actions', 'admin', 'postmeta', 'dynamicfiles', 'daadminwidget', 'markup');
	protected static $instance = null;
	private $version;

	public static function instance(){
		null === self::$instance AND self::$instance = new self;
		return self::$instance;
	}
	private function __construct(){
		$thm = wp_get_theme();
		$this->version = array(wp_get_theme($thm->Template)->Version,$thm->Version);

		add_action( 'after_setup_theme', array($this, 'theme_init') );
		add_action( 'init', array($this, 'init'));
		add_action( 'wp', array($this, 'wp'));
		add_action( 'widgets_init', array($this, 'widgets_init') );
		add_action( 'wp_enqueue_scripts', array($this, 'scripts') );
		add_filter( 'body_class', array($this, 'body_classes') );
		add_action( 'the_post', array($this, 'the_post') );
		add_shortcode('copyright', array($this, 'shortcode_copyright'));
		add_action( 'after_switch_theme', array($this, 'activate') );
		add_filter('nav_menu_css_class', array($this, 'menu_classes'));
		add_filter('excerpt_more', array($this, 'excerpt_more'), 10, 1);
		add_filter('get_the_excerpt', array($this, 'auto_excerpt'), 10, 2);

		add_action( 'pre_get_posts', array($this, 'pre_get_posts') );

		$selected_features = apply_filters('bespoke_f_features', array());
		$selected_features = array_merge(Bespoke::$required_features,$selected_features);

		Bespoke::feature_depends($selected_features);
	}

	public static function feature_depends($features){
		if (empty($features) )
			return;
		if (!is_array($features))
			$reatures = array($features);
		foreach($features as $feature){
			if (in_array($feature, Bespoke::$all_features)) {
				//echo '<br/>feature:'.$feature;
				$file = get_template_directory() .'/lib/bespoke_'.$feature.'.php';
				if (file_exists($file)) {
					require_once ($file);					
				} else {
					require_once (get_template_directory() .'/lib/'.$feature.'/functions.php');				
				}
			}
		}
	}	

	public function shortcode_copyright($atts) {
		$atts = shortcode_atts(array(
				'from' 	=>  false,
			), 
			$atts
		);
		if ($atts['from'] && $atts['from']!=Date('Y')) {
			return '&copy;&nbsp;'.$atts['from'].'-'.Date('Y'); 
		} else {
			return '&copy;&nbsp;'.Date('Y'); 			
		}
	}

	public function version($of='both') {
		if ('child'===$of)
			return $this->version[1];
		else if ('parent'===$of)
			return $this->version[0];
		else
			return join(',',$this->version);
	}

	public function activate() {
	}

	public function theme_init() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on bespoke, use a find and replace
		 * to change 'bespoke' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'bespoke', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		$nav_menus = apply_filters('bespoke_f_nav_menus', array(
			'primary' => esc_html__( 'Primary', 'bespoke' ),
		) );
		if (!empty($nav_menus)) {
			register_nav_menus( $nav_menus );
		}

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_post_type_support('page', 'excerpt');


		/*
		 * Enable support for Post Formats - filterable - default not
		 * See https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		$formats = apply_filters( 'bespoke_f_post_formats', array() );
		if (!empty($formats)) {
			add_theme_support( 'post-formats', $formats );
		}
	}

	public function init() {
		if (apply_filters('bespoke_f_exclude_emojis', true)) {
			remove_action('wp_head', 'print_emoji_detection_script', 7);
			remove_action('admin_print_scripts', 'print_emoji_detection_script');
			remove_action('wp_print_styles', 'print_emoji_styles');
			remove_action('admin_print_styles', 'print_emoji_styles');	
			remove_filter('the_content_feed', 'wp_staticize_emoji');
			remove_filter('comment_text_rss', 'wp_staticize_emoji');	
			remove_filter('wp_mail', 'wp_staticize_emoji_for_email');			
		}
	}

	public function pre_get_posts( $query ) {
	    if ( $query->is_tag() && $query->is_main_query() ) {
	    	$post_types = apply_filters('bespoke_f_taggable_custom_posts', array( 'post' ) );
	        $query->set( 'post_type', $post_types );
	    }
	}

	public function wp() {
		if ( is_search() ) {
			add_action('bespoke_do_loop_header', 'bespoke_markup_search_page_header');			
		} else if ( is_home() && ! is_front_page() ) {
			// blog page (not on home)
			add_action('bespoke_do_loop_header', 'bespoke_markup_blog_page_header');
		} else if (is_archive()) {
			add_action('bespoke_do_loop_header', 'bespoke_markup_archive_page_header');
		}

		if (is_single()) {
			add_action('bespoke_do_after_content', 'bespoke_do_the_post_navigation');
		}
		if (is_archive() || is_home() || is_search()) {
			add_action('bespoke_do_after_loop', 'bespoke_do_the_posts_navigation');
		}
		if (is_singular()) {
			add_action('bespoke_do_after_content', 'bespoke_comments_template', 11);
		}
	}


	public function auto_excerpt( $content, $post ) {
		if (empty($content)) {
			// no shortcodes, apply filters, remove html tags
			$content = strip_shortcodes($post->post_content);
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			$content2 = wp_strip_all_tags($content, true);
			$content = wp_trim_words( $content2, apply_filters( 'excerpt_length', 30 ), '' );
			if (strlen($content) != strlen($content2)) {
				$content .= '&hellip;';
			}
		}
		return $content;
	}

	public function excerpt_more( $content ) {
		global $post;
		return '&nbsp;&hellip;&nbsp;<a class="moretag" href="'. get_permalink($post->ID) . '">read&nbsp;more</a>';
	}

	public function body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}
		return $classes;
	}

	public function widgets_init() {
		if (apply_filters('bespoke_f_register_default_widgetarea', true)) {
			register_sidebar( array(
				'name'          => esc_html__( 'Sidebar', 'bespoke' ),
				'id'            => 'sidebar-1',
				'description'   => esc_html__( 'Add widgets here.', 'bespoke' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			) );
		}
	}

	public function scripts() {
		if (apply_filters('bespoke_f_include_parent_css', false)) {
			wp_enqueue_style( 'bespoke-parent-style', get_template_directory_uri() . '/style.css', array(), $this->version('parent') );
		}
		wp_enqueue_style( 'bespoke-child-style', get_stylesheet_uri(), array(), $this->version('child') );
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array(), $this->version('parent'), false );
		wp_enqueue_script('jquery');
		wp_enqueue_script( 'bespoke-script', get_template_directory_uri() . '/js/bespoke.min.js', array(), $this->version('parent'), true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}

	public function the_post() {
		do_action('bespoke_do_the_post');
	}

	function menu_classes($classes) {
		/* adds 'current_page_parent' *only* to items with class 'menu-item-for-{$post_type}'
		 where $post_type is the current post type */
		$post_type = get_post_type(); 
		foreach (array_keys($classes, 'current_page_parent') as $key) {
			unset($classes[$key]);
		}			
		if (!is_array($post_type)){
			$post_type = array($post_type);
		}
		foreach ($post_type as $pt){
			if(in_array('menu-item-for-'.$pt, $classes))
				array_push($classes, 'current_page_parent');
		}
		return $classes;		
	}
}

global $bespoke;
$bespoke = Bespoke::instance();



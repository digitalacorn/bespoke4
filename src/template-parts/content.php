<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bespoke
 */

if (!defined('ABSPATH')) exit;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
		
		if (!is_single()) {
			bespoke_markup_featured_image( 'thumbnail' );
		}

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php bespoke_markup_post_date(); ?>
			<?php bespoke_markup_author(false); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<?php
		$content_type = apply_filters('bespoke_f_display_excerpt', !is_single()) ? 'excerpt' : 'content';
	?>

	<div class="entry-<?php echo $content_type; ?> clear">
		<?php
			if ( 'excerpt' == $content_type ) {
				the_excerpt();
			} else {
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'bespoke' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bespoke' ),
					'after'  => '</div>',
				) );
			}
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php bespoke_markup_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

<?php
/**
 * Template part for displaying the colophon (footer info).
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bespoke
 */

if (!defined('ABSPATH')) exit;

?>
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'bespoke' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'bespoke' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'bespoke' ), 'bespoke', '<a href="http://www.digitalacorn.co.uk" rel="designer">Digital Acorn Limited</a>' ); ?>
		</div><!-- .site-info -->

var da_bespoke = da_bespoke || (function( window, $, undefined ) {

	function reflow() {
		$.fn.matchHeight._update();
		$.fn.matchHeight._update();	// for 2x height match nesting
	}

	// cookie policy
	function cookie_warning($, options){
		var cookie_warning_content = options.content || '';
		$('body').append('<div id="da_cookie_policy"><div id="cookie_policy_inner">'+cookie_warning_content+'</div></div>');
		var cookie_div_height = $('#da_cookie_policy').outerHeight() -3;
		$('body').addClass('has_cookie_bar');
		$('#cookie_accept').submit(function(e) {
			e.preventDefault();
			$('#da_cookie_policy').fadeOut();
			$('body').removeClass('has_cookie_bar');
			$.cookie(options.cookie, 1, { expires : 2*365, path: '/' });
		});
	};

	var events = {
		url: false,
		click_elm: '.month_change',
		closest: '.da_event_cal_outer_wrapper', 
		init: function (options){
			$.extend(this, options);
			if (events.url)
				events.bind_clicks();
		},
		bind_clicks: function(){
			$(events.click_elm).bind('click.da_event_cal',function(e){
				events.handle_clicks($(this),e);
			});
		},
		handle_clicks: function (clicked, e){
			e.preventDefault();
			var replace_item = clicked.closest(events.closest);
			var month = $(clicked).data('month');
			var year = $(clicked).data('year');
			var calendar_id = $(clicked).data('calendar_id');
			var data = {month : month, year : year, calendar_id : calendar_id};		
			$.get(events.url, data,
				function(returned_html){
					replace_item.replaceWith(returned_html);
					events.bind_clicks();
					da_bespoke.reflow();
				}
			);
		}			
	};

	function fsGallery() {
		var $emptygallery = $('<div class="slickwrapper">');
		var $container;
		var items = [];
		function hide() {
			$('.slickwrapper', $container).replaceWith($emptygallery);
			$container.fadeOut();
		};
		function show(initialSlide) {
			function activate($elem) {
				$elem.attr('src', $elem.data('src'));
			}
			var $gallery = $emptygallery.clone();
			activate($('img', items[initialSlide]));
			for (var i = 0; i < items.length; i++) {
				$gallery.append($('<div>').append(items[i]));
			};
			$('.slickwrapper', $container).replaceWith($gallery);
			$container.show();
			if (items.length>1) {
				$('.slickwrapper', $container).slick({
					centerMode: true,
					slidesToShow: 1,	
					dots: true,
					initialSlide: initialSlide
				});					
			}
			setTimeout(function () {
				$('img', $container).each(function () {
					activate($(this));
				});
			}, 1000);
			$(document).keyup(function(e) {
				if (e.keyCode === 27) {
					hide();
				}
			});
		};
		function add($clickelem, imgurl, caption) {
			var $figure = $('<figure>');
			$figure.append($('<img>').data('src', imgurl));
			$figure.append($('<figcaption>').html(caption));
			$clickelem.data('gallery-pos', items.length);
			items.push($figure);
			$clickelem.click(function (ev) {
				ev.preventDefault();
				show($(this).data('gallery-pos'));
				return false;
			});
		};
		function init(options) {
			$container = $(options.container);
			$("div.gallery figure.gallery-item").each(function () {
				var $this = $(this),
					$clickElem = $('.gallery-icon > a', $this);
				add($clickElem, $clickElem.attr('href'), $('figcaption', $this).html());
			});
			$("#content .entry-content figure a > img").not('div.gallery img').each(function () {
				var $item = $(this).closest('figure'),
					$clickElem = $(this).parent();
				add($clickElem, $clickElem.attr('href'), $('figcaption', $item).html());
			});
			$("#content .entry-content a > img").not('figure img').each(function () {
				var $this = $(this),
					$clickElem = $this.parent();
				add($clickElem, $clickElem.attr('href'), $this.attr('alt'));
			});
			$('.close a', $container).click(function (ev) {
				hide();
				return false;
			});					
		};
		return {
			init: init
		}
	}

	function popupwindow($this) {
		var url = $this.attr('href'),
			attr = $this.data('popup').split(','),
			width = attr[0] || 640,
			height = attr[1] || 480;
		var leftPosition, topPosition;
		leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
		topPosition = (window.screen.height / 2) - ((height / 2) + 50);
		if (leftPosition<=0 || topPosition<=0)
			return true;
		window.open(url, "_blank", "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no");
		return false;
	}

	/*
	// for testing ga
	function ga() {
		console.log('ga', arguments);
	}
	ga.loaded=true;
	*/

	function ga_track_event($this) {
		if ('undefined' !== typeof ga && ga.hasOwnProperty('loaded') && ga.loaded===true) {
			var attr = $this.data('gaevent').split(','),
				ev1 = attr[0] || 'link',
				ev2 = attr[1] || 'click',
				ev3 = attr[2] || $this.attr('href');
			ga('send', 'event', ev1, ev2, ev3);
		}
		return true;
	}

	// document ready
	$ && $(document).ready(function($) {
		$("a[data-gaevent]").click(function () {
			return ga_track_event($(this));
		});
		$("a[data-popup]").click(function () {
			return popupwindow($(this));
		});
	});

	$ && $(document).ready(function($) {
		$.fn.matchHeight._maintainScroll = true;
		$('.grid.equalise').each(function () {
			$(this).find("> [class*='col-'] > .block").matchHeight({byRow: true});
		}); 
	});

	return {
		cookie_warning: cookie_warning,
		events: events,
		reflow: reflow,
		popupwindow: popupwindow,
		fsGallery: fsGallery,
	};
})(window, jQuery);
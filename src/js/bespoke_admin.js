var da_admin = da_admin || (function( window, $, undefined ) {
	var reloaded_widget = false;
	return {
		options: {},
		init: function (options) {
			var self = this;
			$.extend(da_admin.options,options);
			$(document).ready(function($) 
			{
				da_admin.show(0.1,'digitalacorn_dashboard_widget');
				$('.bespoke_field.date input').datepicker({ dateFormat: "dd-mm-yy" }); /* must match serverside format */
				$('#digitalacorn_dashboard_widget').on('click','span.reload_wrap', function(){
					$(this).closest('.inside').slideUp(100);
					$.ajax({
						type : 'POST',
						url : ajaxurl + '?action=bespoke_digitalacorn_dash_invalidate',
						success: function() {
							window.location.reload();
						}
					});
					return false;
				});
			});	
		},
		show: function (i, id) {
			var p, e = $('#' + id + ' div.inside:visible').find('.widget-loading');
			if ( e.length ) {
				p = e.parent();
				setTimeout( function(){
					p.load( ajaxurl + '?action=dashboard-widgets&widget=' + id, '', function() {
						p.hide().slideDown('normal', function(){
							$(this).css('display', '');
						});
					});
				}, i * 500 );
			}
		}
	};
})(window, jQuery);

(function( window, $, undefined ) {	
	/* multiple media select for meta fields */
	var mediaselect_active=false,
	mediaupdate = function ($meta) {
		var $input = $('input', $meta);
		var $thumbs = $('.thumbs', $meta);
		var $post_id = $('.thumbs', $meta).attr('id');
		var ids = $input.val().split(',');

		var buttonlabel = $meta.hasClass('document') ? 'Select Document': 'Edit Media';

		$('.select.button', $meta).html(buttonlabel);
		if (''==$input.val())
		{
			$thumbs.empty();
			$('.clear.button', $meta).addClass('disabled');
		}
		else
		{
			$('.clear.button', $meta).removeClass('disabled');
			$.ajax({
				url : ajaxurl + '?action=bespoke_meta_thumbs&ids='+$input.val()+'&pid='+$post_id+'&field='+$input.attr('name'),
				success: function(data) {
					$thumbs.empty();
					if (data.data.length>0)
					{
						var container = $('<div/>', {'class':'container'});
						_.each(data.data, function (d) {
							container.append(wp.template('da-bespoke-media-meta')(d));
						})
						container.appendTo($thumbs);
					}
				},
				error : function () {
					$thumbs.empty();
				}
			});
		}
	},
	mediaselect = function (e) {
		var $meta = $(this).closest('.bespoke_field');
		var $input = $('input', $meta);

		if ($input.val().length>0) {
			wp.media.gallery.edit('[gallery ids="'+$input.val()+'"]').on('update', function(obj) { 
				var numberlist = []; 
				$.each(obj.models, function(id,val) {numberlist.push(val.id)}); 
				$input.val(numberlist.join(","));
				mediaupdate($meta);
			});
			return;
		}

		if (mediaselect_active)
			return;

		var attr_name = $input.attr('name');

		if ( typeof _ !== 'undefined' && typeof wp !== 'undefined' && wp.media && wp.media.editor )
		{
			var original_send_attachment = wp.media.editor.send.attachment;
			var original_send_to_editor = window.send_to_editor; 
			var media = new Array();

			mediaselect_active = true;

			wp.media.editor.open( attr_name );
			wp.media.frame.modal.on('close', function () {
				// dirty (dirty) solution for close/send event ordering limitation
				window.setTimeout(function () {
					window.send_to_editor = original_send_to_editor;
					wp.media.editor.send.attachment = original_send_attachment;
					mediaupdate($meta);
					mediaselect_active = false;
				}, 250);
			})
			wp.media.editor.send.attachment = function( a, b) {
				media.push(b);
			};
			window.send_to_editor = function(html) {
				$input.val(_.pluck(media, 'id').join());
			}				
		}
	};
	docuselect = function (e) {
		var $meta = $(this).closest('.bespoke_field');
		var $input = $('input', $meta);
		var attr_name = $input.attr('name');

		// configuration of the media manager new instance
		wp.media.frames.da_frame = wp.media({
			title: 'Select Document',
			multiple: false,
			library: {
				type: 'application'
			},
			button: {
				text: 'Use selected Document'
			}
		});
		// Function used for the image selection and media manager closing
		var gk_media_set_attachment = function() {
			var selection = wp.media.frames.da_frame.state().get('selection');
				var media = new Array();
				selection.each(function(attachment) {
					media.push(attachment.attributes.id);
				});
				$input.val(media.join())
				mediaupdate($meta);
		};
		wp.media.frames.da_frame.on('close', gk_media_set_attachment);
		wp.media.frames.da_frame.open(attr_name);
	};
	$(document).ready(function($) 
	{
		$('.bespoke_field.media .select.button').click(mediaselect);
		$('.bespoke_field.media .clear.button').click(function (e) {
			var $meta = $(this).closest('.bespoke_field.media');
			$('input',$meta).val('');
			mediaupdate($meta);
		});
		$('.bespoke_field.media').each(function (index, elem) {mediaupdate($(elem));});
		$('.bespoke_field.document .select.button').click(docuselect);
		$('.bespoke_field.document .clear.button').click(function (e) {
			var $meta = $(this).closest('.bespoke_field.document');
			$('input',$meta).val('');
			mediaupdate($meta);
		});
		$('.bespoke_field.document').each(function (index, elem) {mediaupdate($(elem));});
		//da_docuselect_init('.da-docu-input', '.da-docu-button');
	});
})(window, jQuery);
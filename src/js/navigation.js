/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
( function(window, $, undefined) {
	var $container = $('#site-navigation');
	if (!$container.length) 
		return;
	var $button = $('.menu-toggle', $container);
	var $menu = ('ul', $container).first();
	if(!$menu.length) {
		$button.hide();
	}
	$menu.attr('aria-expanded', false);
	$menu.addClass('nav-menu');
	function menutoggle(val) {
		$container.toggleClass('toggled', !!val );
		$button.attr( 'aria-expanded', !!val );
		$menu.attr( 'aria-expanded', !!val );
	};
	window.addEventListener('resize', function () {
		menutoggle(false);
	}, true);
	$button.click(function () {
		console.log(this);
		menutoggle(!$container.hasClass('toggled'));
	});
	$('ul', $menu).each(function () {
		$(this).parent().attr( 'aria-haspopup', true );
	})
	$('.menu-item-has-children > a, .page_item_has_children > a', $menu).on('click', function (e) {
		e.preventDefault();
		var $elem = $(this).parent();
		if ($elem.hasClass('focus')) {
			$elem.removeClass('focus');
		} else {
			$elem.addClass('focus').siblings().removeClass('focus');
		}
		return false;
	});

})(window, jQuery);
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bespoke
 */

if (!defined('ABSPATH')) exit;

?>

	</div><!-- #main -->
	<?php do_action('bespoke_do_after_main'); ?>
	<?php do_action('bespoke_do_before_footer'); ?>
	<footer id="colophon" class="<?php bespoke_classes('footer', array('site-footer')); ?>" role="contentinfo">
		<?php get_template_part('template-parts/colophon'); ?>
	</footer><!-- #colophon -->
	<?php do_action('bespoke_do_after_footer'); ?>

</div><!-- #page -->
<?php do_action('bespoke_do_after_page'); ?>

<?php 
	wp_footer(); 
	do_action('bespoke_do_after_wp_footer');
	echo "\n".'<script type="text/javascript">'."\n//<![CDATA[\n";
	do_action('bespoke_do_footer_scripts');
	echo ';jQuery && jQuery(document).ready(function($) {'."\n";
	do_action('bespoke_do_js_docready');
	echo "\n});";		
	echo "\n//]]>\n" . '</script>' . "\n";
?>

</body>
</html>

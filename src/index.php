<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bespoke
 */

if (!defined('ABSPATH')) exit;

do_action('bespoke_do_before_output');

get_header(); ?>

	<div id="primary" class="<?php bespoke_classes('primary', array('content-area')); ?>">
		<main id="content" class="" role="main">

		<?php 

		if ( have_posts() ) :

			if ( !is_singular() ) {
				add_action('bespoke_do_before_loop', 'bespoke_do_loop_header');				
			}

			do_action('bespoke_do_before_loop'); 

			while ( have_posts() ) : the_post();

				do_action('bespoke_do_before_content');
				if (apply_filters('bespoke_f_do_default_content_template', true)) {
					bespoke_content_template( is_search() ? 'search' : null );					
				}
				do_action('bespoke_do_after_content');

			endwhile;

			do_action('bespoke_do_after_loop');

		else :

			do_action('bespoke_do_before_loop'); 

			do_action('bespoke_do_before_content');
			if (apply_filters('bespoke_f_do_default_content_template', true)) {
				bespoke_content_template( 'none' );
			}
			do_action('bespoke_do_after_content');

			do_action('bespoke_do_after_loop');

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

if (apply_filters('bespoke_f_do_aside', true)) {
	do_action('bespoke_do_before_aside'); 
		get_sidebar();
	do_action('bespoke_do_after_aside'); 	
}

get_footer();

<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package bespoke
 */

if (!defined('ABSPATH')) exit;

do_action('bespoke_do_before_output');

get_header(); ?>

	<div id="primary" class="<?php bespoke_classes('primary', array('content-area')); ?>">
		<main id="content" class="" role="main">

		<?php 
			do_action('bespoke_do_before_loop'); 
			do_action('bespoke_do_before_content');
		?>

			<section class="error-404 not-found">

			<?php
				$page_header = apply_filters('bespoke_f_page_header', '');
				if ( empty($page_header) ) : ?>
					<header class="page-header">
						<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'bespoke' ); ?></h1>
					</header><!-- .page-header -->
			<?php 
				else:
					echo $page_header;
				endif;
			?>

				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'bespoke' ); ?></p>

					<?php
						get_search_form();

						the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						if ( bespoke_categorized_blog() ) :
					?>

					<div class="widget widget_categories">
						<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'bespoke' ); ?></h2>
						<ul>
						<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
						</ul>
					</div><!-- .widget -->

					<?php
						endif;

						/* translators: %1$s: smiley */
						$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives.', 'bespoke' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );

						the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		<?php 
			do_action('bespoke_do_after_content'); 
			do_action('bespoke_do_after_loop'); 
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

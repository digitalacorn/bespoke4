
# Bespoke 4

## Child Theme construction

It is recommended to not override the following templates - 
index, 404, footer, header. Instead use hooks/filters to add elements in here. Or use template-parts to override default appearance.

Other templates such as single-[posttype] can ovverrule the default template. Either, completely,  to add hooks and then call bespoke_render() to invoke the appropriate template.

## Filters and Actions

actions all start with bespoke_do_
filters all start with bespoke_f_

### Actions

#### Actions within the main rendering of a page

* bespoke_do_before_output
* bespoke_do_before_wp_head
* `wp_head();`
* bespoke_do_after_wp_head
*	bespoke_do_before_page
  *		bespoke_do_before_header
  *		bespoke_do_after_header
  *		bespoke_do_before_main
    *			bespoke_do_before_loop
        *     bespoke_do_loop_header 
    *			`while have_posts();`
      *     bespoke_do_the_post
      *     bespoke_do_before_content
      *     bespoke_do_after_content
    *			bespoke_do_after_loop
    *     bespoke_do_before_aside   
      *			bespoke_do_before_sidebar	
      *       `dynamic_sidebar()`	
      *			bespoke_do_after_sidebar		
    *     bespoke_do_after_aside    
  *		bespoke_do_after_main
  *		bespoke_do_before_footer		
  *		bespoke_do_after_footer		
*	bespoke_do_after_page
*	`wp_footer();`
*	bespoke_do_after_wp_footer
*	bespoke_do_footer_scripts (inside javascript context)
*	bespoke_do_js_docready (inside javascript context, within jQuery docready)

#### Additional actions

* bespoke_do_dynamic_css
* bespoke_do_the_posts_navigation
* bespoke_do_social_shares
* bespoke_do_social_profiles


### Filters

*	bespoke_f_meta_fields
*	bespoke_f_meta_field_visibility
* bespoke_f_global_settings
*	bespoke_f_meta_admin_column_[ID]_title
*	bespoke_f_features
*	bespoke_f_display_excerpt
*	bespoke_f_include_parent_css
*	bespoke_f_post_formats
*	bespoke_f_exclude_emojis
*	bespoke_f_options_sitename
*	bespoke_f_options_icon
*	bespoke_f_content_width
*	bespoke_f_editor_styles
*	bespoke_f_admin_js_dependencies
*	bespoke_f_admin_js_options
*	bespoke_f_editor_css_file
*	bespoke_f_cookie_policy_input_value
*	bespoke_f_classes_[NAME]

  With the following options for [NAME]  
  *	page		default='wrap'
  *	header
  *	main
  * event_day
*	bespoke_f_automatic_attachments_override - deprecated
* bespoke_f_automatic_attachments_enabled
*	bespoke_f_attachment_css
*	bespoke_f_attachment_after_priority
* bespoke_f_attachment_minlistnum
* bespoke_f_do_aside
* bespoke_f_register_default_widgetarea
* bespoke_f_comment_links_in_loop
* bespoke_f_childpagesubnav
* bespoke_f_the_post_navigation_args
* bespoke_f_the_posts_navigation_args
* bespoke_f_taggable_custom_posts
* bespoke_f_favicon_url
* bespoke_f_do_default_content_template
* bespoke_f_edit_post_link_for_page
* bespoke_f_nav_menus

* bespoke_f_og_title
* bespoke_f_og_type
* bespoke_f_og_description
* bespoke_f_og_image

* bespoke_f_social_profiles_content
* da_bespoke_f_social_admin_fields

## Pluggable functions
* da_meta_field

## Tips for upgrading old themes

override pluggable function da_meta_field for metafield names to pick up existing meta entries
filter and action changes..

* bespoke_meta_boxes -> bespoke_f_meta_fields
* dabp_dynamic_CSS -> bespoke_do_dynamic_css
* dabp_do_JS_docready -> bespoke_do_js_docready
* da_events_metafields -> bespoke_f_events_metafields 
* da_events_slug -> bespoke_f_events_slug
* da_events_category_slug -> bespoke_f_events_category_slug
* da_events_extract_meta -> bespoke_f_events_extract_meta
* da_events_status -> bespoke_f_events_status
* da_events_has_event -> bespoke_f_events_day_has_event
* da_event_day_classes -> bespoke_f_classes_event_day
* da_events_day_render_override -> bespoke_f_events_day_render_override
* da_events_archive_meta_query -> bespoke_f_events_archive_meta_query
* dabp_do_JS_footer_scripts -> bespoke_do_footer_scripts
* dabp_do_before_output -> bespoke_do_before_output
* dabp_do_late_head -> bespoke_do_after_wp_head
* da_body_classes -> body_class (but change from string to array)
* dabt_do_before_loop -> bespoke_do_before_loop OR bespoke_do_loop_header
* dabt_do_before -> bespoke_do_before_page
* bespoke_global_settings -> bespoke_f_global_settings
* da_meta_field_visibility -> bespoke_f_meta_field_visibility
* get_my_image_url -> wp_get_attachment_image_src
* STYLESHEET_DIRECTORY -> get_stylesheet_directory_uri()

## Shortcodes

* copyright - displays copyright symbol and date

## Main Navigation

Top level menus with drop-downs need to be 'dummies'. For touch devices. 

## Grid and Blocks

.grid contains a row of columns child columns to be styled with .col-* classes defining widths. Each col should contain a 'div.block'.
.grid.equalise will equalise the heights of .block grandchildren within using jquery.equalizeHeight plugin
.block > .cover will fill the block (useful for global hover and clicks)

## TODO

* attached documents - encapsulation into namespace
* popover gallery
* events
* date check on cookie policy acceptance
* Test Nav menus on touch devices.
* Global options to share with bespoke meta


					
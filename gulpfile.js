var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass');
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	replace = require('gulp-replace'),
	clean = require('gulp-clean'),
	gutil = require('gulp-util');
	phpMinify = require('gulp-php-minify');

	var destination = '/pro/www/wordpress/wp-content/themes/bespoke';
	var rawassets = [
		'./src/**/*.+(jpg|png|tiff|ico|gif)',
	];

    var uglify_options = {
        preserveComments: 'license'
    };

	gulp.task('clean', function(){
	  return gulp.src([destination+'/*'], {read:false})
	  .pipe(clean());
	});

	gulp.task('scripts', function() {
		return gulp.src([
            './src/js/modernizr.js',
            './src/js/bespoke_admin.js',
            './src/js/jquery.cookie.js',           
            './node_modules/slick-carousel/slick/slick.js'            
		])
		.pipe(uglify(uglify_options))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(destination+'/js'));
	});

	gulp.task('mainscript', function() {
		return gulp.src([
            './src/js/bespoke.js',
            './src/js/navigation.js',
            './src/js/accessibility.js',
            './src/js/jquery.matchHeight.js'           
        ])
		.pipe(uglify(uglify_options).on('error', gutil.log))
		.pipe(concat('bespoke.min.js', {newLine: ';\n'}))
		.pipe(gulp.dest(destination+'/js'));
	});

 //    gulp.task('vendorstyles', function() {
	// 	return gulp.src('./src/vendor.scss')
	// 	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
	// 	.pipe(replace('/*!', '/*'))
	// 	.pipe(replace('!*/', '*/\n'))
	// 	.pipe(rename({suffix: '.min'}))		
	// 	.pipe(gulp.dest(destination+'/lib/css'))
	// });

    gulp.task('styles', function() {
		return gulp.src('./src/css/admin.css')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(replace('/*!', '/*'))
		.pipe(replace('!*/', '*/\n'))
		.pipe(gulp.dest(destination+'/css'))
	});


    gulp.task('mainstyle', function() {
		return gulp.src('./src/css/style.scss')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(replace('/*!', '/*'))
		.pipe(replace('!*/', '*/\n'))
		.pipe(gulp.dest(destination))
	});

    gulp.task('fontawesomefonts', function() {
	  return gulp.src('./node_modules/font-awesome/fonts/*', { base: './node_modules/font-awesome/' })
	  	.pipe(gulp.dest(destination));
    });

    gulp.task('fontawesome', ['fontawesomefonts'], function() {
		return gulp.src('./src/css/font-awesome.scss')
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(replace('/*!', '/*'))
		.pipe(replace('!*/', '*/\n'))
		.pipe(gulp.dest(destination+'/css'))
	});

	gulp.task('movephp', function(){
	  return gulp.src('./src/**/*.php', { base: './src/' })
	  //.pipe(phpMinify())
	  .pipe(gulp.dest(destination));
	})	

	gulp.task('rawassets', function(){
	  return gulp.src(rawassets, { base: './src/' })
	  .pipe(gulp.dest(destination));
	})	

 //    gulp.task('fonts', function() {
	// 	return gulp.src('bower_components/font-awesome/fonts/*')
	// 	.pipe(gulp.dest(destination+'/lib/font'))
	// });

gulp.task('default', ['styles', 'mainstyle', 'mainscript', 'scripts', 'movephp', 'rawassets', 'fontawesome']);

gulp.task('watch', ['default'], function() {
  // Watch .scss files
  gulp.watch('./src/**/*.+(scss|css)', ['styles', 'mainstyle', 'fontawesome']);
  // Watch .js files
  gulp.watch('./src/**/*.js', ['scripts', 'mainscript']);
  // watch .php files
  gulp.watch('./src/**/*.php', ['movephp']);

  gulp.watch(rawassets, ['rawassets']);

});

module.exports = gulp;

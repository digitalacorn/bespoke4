<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bespoke
 */

if (!defined('ABSPATH')) exit;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">CLUBBY CLUB CLUB ', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">CLUBBY CLUB CLUB LISTING ', '</a></h2>' );
			}
		?>
	</header><!-- .entry-header -->

	<div class="entry-content clear">

		<div class="slideshow starthidden clubslideshow">
		  <div>Slide1</div>
		  <div>Slide2</div>
		  <div>Slide3</div>
		</div>

		<?php

			BespokeCarousel::activate('.clubslideshow', array('dots'=>true));


			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'bespoke' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bespoke' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php bespoke_markup_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

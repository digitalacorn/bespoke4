<?php
	
	if (!defined('ABSPATH')) exit;

	add_filter('bespoke_f_options_sitename', create_function('$content', 'return "Example Child";'));

	add_filter('bespoke_f_include_parent_css', '__return_true');
	add_filter('bespoke_f_features', 'requested_features');
	function requested_features($features) {
		$features[] = 'security';
		$features[] = 'cookiewarning';
		$features[] = 'attachments';
		$features[] = 'carousel';
		$features[] = 'optimise';
		$features[] = 'events';
		$features[] = 'fontawesome';
		return $features;
	}

	add_filter('bespoke_f_global_settings',  'child_global_settings', 10, 1);

	function child_global_settings($settings) {
		$settings['text_field'] = array(
			'input_type' 	=> 'input',
			'name' 			=> 'global text',						
			'description' 	=> 'this is a global text field'
		);
		return $settings;
	}

	add_filter('bespoke_f_meta_fields', 'meta');
	function meta($metafields){

		$meta = array();

		$meta['show_on_homepage'] = array(
			'default' => '',
			'input_type' => 'checkbox',
			'priority' => 1,
			'name' => 'Show On Homepage',
			'post_types' => array('page'),
			'description' => 'Check this box to display on the homepage'
		);

		$meta['club_phone'] = array(
			'default' => '',
			'input_type' => 'input',
			'priority' => 1,
			'name' => 'Phonenumber',
			'post_types' => array('club'),
			'description' => 'the contact phone number'
		);

		return array_merge($metafields, $meta);
	}

	add_action('init', 'register_posttypes');
	function register_posttypes() {
		$pt_args = array(
			'labels' => auto_post_type_labels('Club', 'Clubs'),
			'public' => true,
			'description' => '',
			'publicly_queryable' => true,
			'show_ui' => true, 
			'show_in_menu' => true, 
			'query_var' => true,
			'rewrite' => array('slug' => 'club'),
			'capability_type' => 'post',
			'has_archive' => true, 
			'hierarchical' => false,
			'menu_icon' =>  get_template_directory_uri() . '/img/favicon.ico',
			'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'wpautop')
		); 
		register_post_type('club', $pt_args);		
	}


/*
	uncomment this to overrule the default cookie warning styling
	function cookie_warning_css(){
	}
*/
